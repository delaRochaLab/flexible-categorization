"""
@author: genis
This script computes the psychopysical kernel of the subjects
in Bronfman et al. 2016
"""

import sys
sys.path.insert(0, '/home/genis/flexible-categorization/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt
import pandas as pd



df=pd.read_pickle('/home/genis/flexible-categorization/figure6/experimental_data.pickle')
# trial_durations=[10,20,30] #exp 1
# exp_num=1
trial_durations=[50] #exp 4
exp_num=4

# trial_durations=[10,20,30,50] #all
# exp_num="all_blocks"

kernels=[]
kernels_PI=[]
kernels_shuffle=[]
plt.figure()
Nboot=1000
error=True
area=np.zeros((2,len(trial_durations)))
PRI=np.zeros((2,len(trial_durations)))

area_PI=[]
for itd,td in enumerate(trial_durations):
    if exp_num=='all':
        aux_df=df[ (df.trial_duration==td)   ]  
        
    elif exp_num=='all_blocks':
        aux_df=df[ (df.trial_duration==td) & (df.exp_num!=3)  ]  
    else:
        aux_df=df[ (df.trial_duration==td) & (df.exp_num==exp_num)]       
        
    stim=np.vstack( np.array( aux_df.stim-aux_df.stim_mean) )
    d=np.array(aux_df.decision)
    ker=ba.kernel_err(stim,d,error=error,Nboot=Nboot)
    kernels.append(ker)   
    
    d_PI=[]
    stim_R=np.vstack( np.array( aux_df.stim_R )) 
    stim_L=np.vstack( np.array( aux_df.stim_L ))
    for itrial in range(len(stim_R)):
        if np.sum(stim)>0:
            d_PI.append(1)
        else:
            d_PI.append(-1)
        
    kernels_PI.append(ba.kernel_err(stim,np.array(d_PI),error=error,Nboot=Nboot))
    d_shuffle=d.copy()
    np.random.shuffle(d_shuffle)  
    aux=ba.kernel_err(stim,d_shuffle,error=error,Nboot=Nboot)
    kernels_shuffle.append(aux)
    area_PI.append( td*(0.5+2/np.pi*np.arctan(1/np.sqrt(2*td-1)) ) )
    
  


f=open('/home/genis/flexible-categorization/figure6/kernel_all_data_exp'+str(exp_num)+'.pickle','wb')
data={}
data['kernels']=kernels
data['kernels_PI']=kernels_PI
data['kernels_shuffle']=kernels_shuffle
data['trial_durations']=trial_durations

pickle.dump(data,f)
f.close()

