#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 20 10:53:19 2018

@author: gprat
This script computes the psychophysical kernels of the dw model with 
the data from Bronfman et al. 2016
"""

import sys
sys.path.insert(0, '/home/genis/flexible-categorization/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt
import pandas as pd


path='/home/genis/flexible-categorization/figure6/'

df=pd.read_pickle(path+'experimental_data.pickle')
exp_num=[1,4]
df=df[(df.exp_num==1) | (df.exp_num==4) ]

mu=-0.2
td=[10,20,30,50]
c2=1.6
c4=4
sigma0=0.3
tau=200
dt=tau/40.
Tframe=100.
NT=int(Tframe/dt)


Nboot=1000
coef=np.array([0,c2,c4])
Performance=np.zeros(len(td))
Performance_error=np.zeros(len(td))
kernels=[]
z=1.96
fig, axes = plt.subplots(nrows=1, ncols=6,figsize=(10/2.54,15/2.54))

area=np.zeros((2,len(td)))
pri=np.zeros((2,len(td)))




for itd,trial_duration in enumerate(td):
    df_aux=df[df.trial_duration==trial_duration]
    cd=-1*np.array(df_aux.correct_decision)
    stim=np.vstack( np.array( df_aux.stim) )
    Nstim=len(stim)
    internal_noise=sigma0*np.random.randn(Nstim,NT*trial_duration)
    d,_=sd.simulation_frames_tau(coef,stim,internal_noise,Tframe,tau,0.0)
    d=np.array(d)
    Performance[itd],Performance_error[itd]= ba.mean_and_error( (d*cd+1.)/2 )
    stim=np.vstack( np.array( df_aux.stim-df_aux.stim_mean) )

    aux_k=ba.kernel_err(stim-mu,d,error=True,Nboot=Nboot)
    kernels.append(aux_k)
    
    
    
plt.figure()
plt.errorbar(td,Performance,yerr=Performance_error,fmt='o-',color='black')

for ikernel in range(len(kernels)):
    t=range(len(kernels[ikernel]['kernel']))
    axes[ikernel].plot(t,kernels[ikernel]['kernel'],'-ok')


axes[4].plot(td,area[0],'o-')
axes[5].plot(td,pri[0],'o-')




data={}
data['kernels']=kernels
data['trial_durations']=np.array(td)/0.05
data['Accuracy_model']=Performance
data['Accuracy_error_model']=Performance_error
    
data['coef']=coef
data['sigma0']=sigma0


f=open(path+'pk_dw.pickle','wb')
pickle.dump(data,f)
f.close()










