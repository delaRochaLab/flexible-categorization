"""

@author: genis
This script transforms matlab files into pickles. It can be used with the data from: 
https://doi.org/10.1371/journal.pcbi.1004667
(Bronfman et al. 2016)
"""


import numpy as np
import scipy.io as sio
import pickle
import os
import glob
import matplotlib.pyplot as plt
import pandas as pd

filename='/home/genis/flexible-categorization/figure6/AllData_Exp1.mat'

data={}
decisions=[]
correct_decision=[]
stim=[]
stim_R=[]
stim_L=[]

sub=[]
mean=[]
trial_duration=[]
exp_num=[]
condition=[]
perturbation_tw=[]
perturbation_congruent=[]
#path='/home/genis/Dropbox/att_behavioral_data/data_S'+subject_initials
#path='./'
sub_num=0
for filename in glob.glob(os.path.join(os.getcwd(), '/home/genis/flexible-categorization/figure6/AllData_Exp*.mat')):
    print(filename)
    mat_contents = sio.loadmat(filename)
    for isub in range(len(mat_contents['AllData'][0])):
    #    decisions.append([])
    #    correct_decision.append([])
        print(sub_num)
        for itrial in range(len(mat_contents['AllData'][0][isub][4])):
            aux_d=mat_contents['AllData'][0][isub][4][itrial][0]
            aux_cd=mat_contents['AllData'][0][isub][3][itrial][0]
            if (aux_d==aux_d and aux_cd==aux_cd):
                decisions.append(np.sign(aux_d-1.5)) #transform form 1,2 to -1,1
                correct_decision.append(np.sign( (aux_cd-1.5) ) ) #1 is Left 2 is right, I transform to -1 left 1 right
                mean.append( correct_decision[-1]*0.1) #mean of the stimulus  0.15 is 075 -0.65 from Bronfman et al 2016 pag 17
                #real means of the stimuli are different 0.1 instead of 0.15.
                condition.append( mat_contents['AllData'][0][isub][0][itrial][0] )
                
                aux=mat_contents['AllData'][0][isub][2][itrial][:,0]
                aux=aux[~np.isnan(aux)]
                stim_L.append(aux)
                
                aux=mat_contents['AllData'][0][isub][2][itrial][:,1]
                aux=aux[~np.isnan(aux)]
                stim_R.append(aux)
                
                aux=mat_contents['AllData'][0][isub][2][itrial][:,1]-mat_contents['AllData'][0][isub][2][itrial][:,0] #stim 1D is stim_R-stimL
                
                aux=aux[~np.isnan(aux)]
                
                
                stim.append(aux)
                trial_duration.append(mat_contents['AllData'][0][isub][7][itrial][0])
                
                perturbation_tw.append(mat_contents['AllData'][0][isub][8][itrial][0]) 
                
                perturbation_congruent.append(mat_contents['AllData'][0][isub][5][itrial][0])
                
                sub.append(sub_num)
                exp_num.append(int(filename[-5]))
        sub_num=sub_num+1
        
data['decision']=decisions
data['correct_decision']=correct_decision
data['stim_mean']=mean
data['stim']=stim
data['stim_L']=stim_L
data['stim_R']=stim_R


data['trial_duration']=trial_duration
data['sub_num']=sub
data['exp_num']=exp_num
data['condition']=condition

    
data['perturbation_tw']=perturbation_tw
data['perturbation_congruent']=perturbation_congruent


df_data=pd.DataFrame.from_dict(data)
pd.to_pickle(df_data,'/home/genis/flexible-categorization/figure6/experimental_data.pickle')





