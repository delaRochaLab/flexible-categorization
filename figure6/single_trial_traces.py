
"""
@author: genis
This script computes single trial traces of the dw model solving
the task described in Bronfman et al. 2016 
"""

import sys
sys.path.insert(0, '/home/genis/flexible-categorization/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt
import pandas as pd


path='/home/genis/flexible-categorization/figure6/'

df=pd.read_pickle(path+'experimental_data.pickle')
exp_num=[1,4]
df=df[(df.exp_num==1) | (df.exp_num==4) ]

mu=-0.2
td=[10,20,30,50]
c2=1.6
c4=4
sigma0=0.3
tau=200
dt=tau/40.
Tframe=100.
NT=int(Tframe/dt)


Nboot=1000
coef=np.array([0,c2,c4])
Performance=np.zeros(len(td))
Performance_error=np.zeros(len(td))
kernels=[]
z=1.96
fig, axes = plt.subplots(nrows=1, ncols=6,figsize=(10/2.54,15/2.54))

TRACES=[]
STIM=[]
INTERNAL_NOISE=[]
Nstim=20

for itd,trial_duration in enumerate(td):
    df_aux=df[df.trial_duration==trial_duration]
    cd=-1*np.array(df_aux.correct_decision)

    stim=np.vstack( np.array( df_aux.stim) )
    stim=stim[:Nstim,:]
 
    internal_noise=sigma0*np.random.randn(Nstim,NT*trial_duration)
    d,xtraces=sd.simulation_frames_tau_traces(coef,stim,internal_noise,Tframe,tau,0.0)
    xtraces=np.array(xtraces)
    TRACES.append(xtraces)
    STIM.append(stim)
    INTERNAL_NOISE.append(internal_noise)


i_stim=3
iT=1
plt.figure()
t=np.linspace(0,iT+1,len(TRACES[iT][i_stim]))
plt.plot(t,TRACES[iT][i_stim])
plt.figure()

t=np.linspace(0,iT+1,len(INTERNAL_NOISE[iT][i_stim]))

plt.plot(t,INTERNAL_NOISE[iT][i_stim])
plt.figure()
s=STIM[iT][i_stim]
stim_plot=[val for val in s for _ in range(2)]
ts_plot=np.linspace(0,iT+1,len(stim_plot))
plt.plot(ts_plot,stim_plot)
plt.show()

data={}
data['INTERNAL_NOISE']=INTERNAL_NOISE
data['TRACES']=TRACES
data['STIM']=STIM

f=open(path+'single_trial_traces.pickle','wb')
pickle.dump(data,f)
f.close()










