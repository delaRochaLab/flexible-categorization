#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 17 15:51:36 2018

@author: genis
"""

import sys
sys.path.insert(0, '/home/genis/flexible-categorization/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt

plt.rcParams['svg.fonttype'] = "none"
plt.rcParams['pdf.fonttype'] = 'type3'


path='/home/genis/flexible-categorization/figure6/'


fontsize=8
    
exp_num=[1,4]

tmin=10
tmax=50
conf_interval=1.96
fig, axes = plt.subplots(nrows=3, ncols=4,figsize=(11/2.54,11/2.54))
kernels=[]
kernels_shuffle=[]
area=[[],[]]
pri=[[],[]]
trial_durations=[]

color_T=["#262932","#4A5369","#6C81AB","#89B3F8"]

vmin=0
vmax=60
hs=0.02
hs2=0.07
h0=0.15
hf=0.1
h_traces=(1-h0-hf-2*hs-hs2)/4.
w_traces=0.1279
w0=0.13
ax1=fig.add_axes([w0,h0,w_traces,h_traces])
ax2=fig.add_axes([w0,h0+hs+h_traces,w_traces,h_traces])
ax3=fig.add_axes([w0,h0+(hs+hs2+2*h_traces),w_traces,h_traces])
ax4=fig.add_axes([w0,h0+(2*hs+hs2+3*h_traces),w_traces,h_traces])


f=open(path+'single_trial_traces.pickle','rb')
data=pickle.load(f)
INTERNAL_NOISE=data['INTERNAL_NOISE']
TRACES=data['TRACES']
STIM=data['STIM']
iT=2
itrial=10
t=np.linspace(0,iT+1,len(TRACES[iT][itrial])-1)
ax2.plot(t,TRACES[iT][itrial][1:],'k-')

s=STIM[iT][itrial]
ts_plot=np.linspace(0,iT+1,len(s))
ax1.plot(ts_plot,np.zeros(len(ts_plot)),'--',color="grey",lw=1)
ax2.plot(ts_plot,np.zeros(len(ts_plot)),'--',color="grey",lw=1)

ax1.step(ts_plot,s,'k-')
hp.xticks(ax1,[0,(iT+1.)/2.,iT+1],fontsize=fontsize)




iT=0
itrial=4
t=np.linspace(0,iT+1,len(TRACES[iT][itrial])-1)
ax4.plot(t,TRACES[iT][itrial][1:],'k-')

s=STIM[iT][itrial]
ts_plot=np.linspace(0,iT+1,len(s))
ax3.plot(ts_plot,np.zeros(len(ts_plot)),"--",color="grey",lw=1)
ax4.plot(ts_plot,np.zeros(len(ts_plot)),"--",color="grey",lw=1)
ax3.step(ts_plot,s,'k-')
hp.xticks(ax3,[0,(iT+1.)/2.,iT+1],fontsize=fontsize)

hp.yticks(ax4,[-1,0,1],fontsize=fontsize)
hp.yticks(ax3,[-0.7,0,0.7],fontsize=fontsize)
hp.yticks(ax2,[-1,0,1],fontsize=fontsize)
hp.yticks(ax1,[-0.7,0,0.7],fontsize=fontsize)


hp.remove_axis_bottom(ax4)
hp.remove_axis_bottom(ax2)
hp.remove_axis(ax3)
hp.remove_axis(ax1)

ax1.set_ylim([-0.7,0.7])
ax2.set_ylim([-1,1])
ax3.set_ylim([-0.7,0.7])
ax4.set_ylim([-1,1])


ax1.set_xlim([0,3])
ax2.set_xlim([0,3])
ax3.set_xlim([0,1])
ax4.set_xlim([0,1])

ax3.set_ylabel("Stimulus",fontsize=fontsize)
ax4.set_ylabel("Decision \n variable "+r"$x$",fontsize=fontsize)
ax1.set_xlabel('Time (s)',fontsize=fontsize)
ax3.set_xlabel('Time (s)',fontsize=fontsize)

for iexp in range(len(exp_num)):

    f=open(path+'kernel_all_data_exp'+str(exp_num[iexp])+'.pickle','rb')
    data=pickle.load(f)
    for i in range(len(data['kernels'])):
        kernels.append(data['kernels'][i])
        kernels_PI=data['kernels_PI'][i]
        kernels_shuffle.append(data['kernels_shuffle'][i])
        trial_durations.append(data['trial_durations'][i])
    f.close()


for i in range(len(kernels)):
    t=range(len(kernels[i]['kernel']))
    axes[0][i].plot(t,np.zeros(len(t))+0.5,'--',color="grey",lw=1)
    axes[0][i].plot(t,kernels[i]['kernel'],'-',color=color_T[i])
    axes[0][i].fill_between(t, kernels[i]['kernel']-conf_interval*kernels[i]['error'],kernels[i]['kernel']+conf_interval*kernels[i]['error'],alpha=0.5,color=color_T[i])

    axes[0][i].set_ylim([0.43,.7])
    if i==0:
        hp.yticks(axes[0][i],[0.5,0.6,0.7],fontsize=fontsize)
    else:
        hp.yticks(axes[0][i],[])
        axes[0][i].spines['left'].set_visible(False)
    hp.xticks(axes[0][i],[0,t[-1]/2.0,t[-1] ], ["","","",],fontsize=fontsize )
    hp.remove_axis(axes[0][i])


########################## MODEL ################


f=open(path+'pk_dw.pickle','rb')

data=pickle.load(f)
f.close()
kernels=data['kernels']

for i in range(len(kernels)):
    c=color_T[i]
    t=range(len(kernels[i]['kernel']))
    axes[1][i].plot(t,np.zeros(len(t))+0.5,'--',color="grey",lw=1)
    axes[1][i].plot(t,kernels[i]['kernel'],'-',color=c)
    axes[1][i].fill_between(t, kernels[i]['kernel']-conf_interval*kernels[i]['error'],kernels[i]['kernel']+conf_interval*kernels[i]['error'],alpha=0.5,color=c)


    axes[1][i].set_ylim([0.4,.7])
    hp.remove_axis(axes[1][i])
    if i==0:
        hp.yticks(axes[1][i],[0.5,0.6,0.7],fontsize=fontsize)
    else:
        hp.yticks(axes[1][i],[])
        axes[1][i].spines['left'].set_visible(False)
        
    hp.xticks(axes[1][i],[0,t[-1]/2.,t[-1] ],[0,str(trial_durations[i]/20.),str(trial_durations[i]/10)],fontsize=fontsize)
    if i==0:
        axes[1][i].set_xlabel('Time (s)',fontsize=fontsize)

axes[1][0].set_ylabel('Stimulus impact',fontsize=fontsize)


hf=0.05
hs=0.05
w0=0.15
wf=0.01
ws=0.15
ws2=0.02
hs2=0.07 #space between kernels and traces
w=(1-w0-wf-3*ws2)/4.
h=(0.6-hf-hs-hs2)/2.
  
w_pos=[w0, w0+w+ws2,  w0+2*(ws2+w),  w0+3*(ws2+w) ]
h_pos=[1-hf-h,1-hf-hs-2*h]

for iax in range(len(axes)-1):
    for iax2 in range(len(axes[0])):
        axes[iax][iax2].set_position( [w_pos[iax2],h_pos[iax],w,h] )


ws1=0.1
ws1_2=0.05
w=(1-w0-wf-ws)/4.

h0=0.1
h=(0.35-h0-hs)/2.0


ax3.set_position([w0,h0,w,h])
ax4.set_position([w0,h0+hs+h,w,h])
ax1.set_position([w0+ws+w,h0,3*w,h])
ax2.set_position([w0+ws+w,h0+hs+h,3*w,h])



axes[2][0].axis("off")
axes[2][1].axis("off")
axes[2][2].axis("off")
axes[2][3].axis("off")


fig.savefig(path+'figure6_pyhton.svg')

plt.show()
