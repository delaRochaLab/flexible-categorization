# Flexible categorization in perceptual decision making #

This repository contains all the code to reproduce the figures from Prat-Ortega et al. Nat comms, In press.

## Structure ##


The folder functions contains 3 scripts with all the functions to produce the figures:

1.analysis_fc.py: Functions to analyze behaviour data (psychophysical kernels,psychometric curves...)

2.help_plot.py: Plotting functions.

3.simulations_fc.pyx: Cython functions to simulate the different diffusion models.


Each folder figureX contains all the necessary scripts to produce figure X. Specifically the file figureX/figureX.py produces
figure X. The rest of the scripts run simulations or intermediate analyses. 

The expected run time of the simulations scripts depends on the number of trials. It is typically less than 10 min for 1e5 trials. 

The code to run and analyze the simulations of the spiking network from figure 5 can be found here: 
https://github.com/wimmerlab/flexcat-spiking


## System Requirements ##
#### Hardware Requirements ####
The scripts require only a standard computer with enough ram memory.

#### Software Requirements ###

##### OS Requirements #####

The code has been developed and tested with Ubuntu 18.04.4

##### Python dependencies #####

All the code is written in Python 3 and it only depends on scientific python standard libraries:

	numpy
	scipy
	matplotlib
	Cython
	pandas


## Intallation guide ##

Before running any of the scripts, the cython code needs to be compiled:

	python setup_simulations_fc.py build_ext --inplace


## License ##
GNU GENERAL PUBLIC LICENSE


Contact details: genisprat@gmail.com
