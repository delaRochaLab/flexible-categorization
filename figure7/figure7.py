#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 17 15:51:36 2018

@author: genis
"""

import sys
sys.path.insert(0, '/home/genis/flexible-categorization/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt
import fit_dw_fc as fdw
from scipy.stats import norm



plt.rcParams['svg.fonttype'] = "none"
plt.rcParams['pdf.fonttype'] = 'type3'


path='/home/genis/flexible-categorization/figure7/'


fig, axes = plt.subplots(nrows=1, ncols=4,figsize=[17.85/2.54,4.5/2.54])
axes=np.hstack(axes)
fontsize=8
# fig2, axes2 = plt.subplots(nrows=1, ncols=2)


################ COLORS ################3

colors_mu=[ "#762a83","#af8dc3","#e7d4e8","#f7f7f7","#d9f0d3","#7fbf7b","#1b7837"]

color_sigma=["#560105","#8B1518","#C1332D","#F55546"]
color_sigma_i=["#4D3F07","#826C17","#BB9D2C","#F8D046"]

mu_small=0.04
mu_medium=0.08
mu_high=0.16


f=open(path+'optimizePC_parameter_fdw.pickle','rb')
data_save=pickle.load(f,encoding="latin1")
f.close()

ibest=data_save['ibest']
ibest=3907   #I am using this because the ibest is not a maximum this it is
param=data_save['final_res'][ibest].x
PC_theory=data_save['final_PC_fit'][ibest]

mu_small_logit=3.2
mu_medium_logit=6.4
mu_high_logit=12.8
MU=[ [3.2,3.2],[3.2,6.4],[3.2,12.8],[6.4,3.2],[6.4,6.4],[6.4,12.8],[12.8,3.2],[12.8,6.4],[12.8,12.8] ]
kmu_fit=param[0]
c2_fit=param[1]
sigma_fit=param[2]
tau_fit=param[3]



##############TRACES############
f=open(path+'internal_noise_fdw.pickle','rb')
data_save=pickle.load(f)
f.close()
sigma_s=data_save['stimulus_noise']
sigma_i=data_save['internal_noise']

print(sigma_fit, np.sqrt(sigma_s**2+sigma_i**2) )
T=120
Tinter=360
deltat=tau_fit/40.
dt_sqrt=np.sqrt(deltat)
NT=int(round(T/deltat))
NTinter=int(round(Tinter/deltat))

imu=4
Nstim=50
Npulses=2

np.random.seed(176589234)
stim_noise=sigma_s*np.random.randn(Nstim,Npulses,NT)
internal_noise=sigma_i*np.random.randn(Nstim,2*NT+NTinter)

coef=np.array([0,c2_fit,1.])
coef_No_stim=np.array([0,c2_fit,1.])


stim=np.repeat( np.array([[kmu_fit*MU[imu][0],kmu_fit*MU[imu][1] ]]), Nstim,axis=0)

d,x=sd.simulation_frames_tau_delay_traces(coef , coef_No_stim,  stim,  internal_noise, stim_noise, T ,Tinter,tau_fit,0.0)
x2=np.array(x[19])
box=axes[0].get_position()
hs=0.01


h0=0.25
hf=0.05
w0=.065
wf=0.02
ws=.1
h=1-h0-hf
hs=0.02
h_traces=(1-h0-hf-2*hs)/3.
w_traces=(1-w0-wf-3*ws)/4.

ax1=fig.add_axes([w0,h0,w_traces,0.9*h_traces])
ax2=fig.add_axes([w0,h0+0.9*h_traces+hs,w_traces,0.9*h_traces])
ax3=fig.add_axes([w0,h0+0.9*2*h_traces+2*hs,w_traces,1.4*h_traces])

for  i in range(len(axes)):
    axes[i].set_position([w0+i*(ws+w_traces),h0,w_traces,h])


start_traces=-500
x2=np.concatenate((np.zeros(2),x2))
t=np.concatenate( (np.array([start_traces,0]),np.array(range(len(x2)-2)) ) )
ax3.plot(t,x2,'k-',lw=0.5)
hp.xticks(ax3,[0,NT,NT+NTinter],[0,T,T+Tinter],)
hp.yticks(ax3,[0],fontsize=fontsize)
hp.yticks(ax2,[0],fontsize=fontsize)
hp.yticks(ax1,[0],fontsize=fontsize)

run_w=20
internal_noise_plot=ba.running_average( internal_noise[19],run_w,mode='valid')
internal_noise_plot=np.concatenate( (np.zeros(2),internal_noise_plot) )
stim_plot=ba.running_average(np.concatenate((kmu_fit*MU[imu][0]+stim_noise[19][0],np.zeros(NTinter),kmu_fit*MU[imu][1]+stim_noise[19][1])),run_w,mode='valid')
stim_plot=np.concatenate( (np.zeros(2),stim_plot))
t=np.concatenate( (np.array([start_traces,0]),np.array(range(len(internal_noise_plot)-2)) ) )
ax2.plot(t,internal_noise_plot,'-',color=color_sigma_i[-1])
ax1.plot(t,stim_plot,'-',color=color_sigma[-1])

hp.remove_axis_bottom(ax3)
hp.remove_axis_bottom(ax2)
hp.remove_axis(ax1)
ax1.set_ylabel("stim",fontsize=fontsize)
ax2.set_ylabel("internal \n noise",fontsize=fontsize)
ax3.set_ylabel(r'$x$',fontsize=fontsize)


ax1.set_xlabel('Time (ms)',fontsize=fontsize)

hp.xticks(ax1,[0,NT,NT+NTinter],["0",str(T),str(T+Tinter)],fontsize=fontsize)
ax1.set_ylim(-0.5,0.5)
ax2.set_ylim(-0.5,0.5)
axes[0].axis("off")



################# PRI vs c2 #####################################
markersize=3

f=open(path+'PRI_c2.pickle','rb')
data2=pickle.load(f)
f.close()
C2=data2['C2']
PRI_c2=data2['PRI']
c2_fit=data2['c2_fit']
PRI_fit=data2['PRI_fit']

f=open(path+'PRI_data.pickle','rb')
data_PRI=pickle.load(f)
f.close()
lconf=data_PRI['conf_interval'][0]
hconf=data_PRI['conf_interval'][1]
PRI_data=data_PRI['PRI']
axes[2].fill_between([C2[0]/2,C2[-1]/2],[-lconf,-lconf],[-hconf,-hconf],color='black',alpha=0.3)

axes[2].plot(C2/2.,-PRI_c2,'k-')
axes[2].plot(c2_fit/2,-PRI_fit,'ko',markersize=markersize)
axes[2].plot([c2_fit/2.,c2_fit/2.],[-1,-PRI_fit],'--',color="grey")

axes[2].plot([C2[0]/2.,C2[-1]/2.0],[-PRI_data,-PRI_data],'k--')
axes[2].set_ylim([-1,1])
axes[2].set_xlim([0.3,1.25])


axes[2].set_xlabel(r"$\alpha$",fontsize=fontsize)
axes[2].set_ylabel('Primacy-Recency Index',fontsize=fontsize)
hp.remove_axis(axes[1])
hp.yticks(axes[2],[-1,0,1],fontsize=fontsize)

hp.xticks(axes[2],[0.3,0.8,1.3],fontsize=fontsize)




################# Performancce vs mu2 plot #####################################

f=open(path+'data_kiani_Maryam.pickle','rb')
data_data=pickle.load(f)
f.close()
PC_data=data_data['PC']
PC_data_error=data_data['PC_error']

colors=[colors_mu[2],colors_mu[1],colors_mu[0]]

coh2=[0,1,2]
aux_1=-1
z=1.96
shift=0.2
axes[1].errorbar([aux_1],PC_data[0],yerr=z*PC_data_error[0],fmt='s',color=colors[0],markersize=markersize)

axes[1].errorbar([aux_1+shift],PC_data[1],yerr=z*PC_data_error[1],fmt='s',color=colors[1],markersize=markersize)
axes[1].errorbar([aux_1],PC_data[2],yerr=z*PC_data_error[2],fmt='s',color=colors[2],markersize=markersize)

axes[1].errorbar(coh2,PC_data[5:8],yerr=z*PC_data_error[5:8],fmt='o',color=colors[0],markersize=markersize)
axes[1].errorbar(coh2,PC_data[8:11],yerr=z*PC_data_error[8:11],fmt='o',color=colors[1],markersize=markersize)
axes[1].errorbar(coh2,PC_data[11:14],yerr=z*PC_data_error[11:14],fmt='o',color=colors[2],markersize=markersize)

axes[1].plot(coh2,PC_theory[5:8],'-',color=colors[0],label="3.2 %")
axes[1].plot(coh2,PC_theory[8:11],'-',color=colors[1],label="6.4 %")
axes[1].plot(coh2,PC_theory[11:14],'-',color=colors[2],label="12.8 %")
axes[1].legend(frameon=False,fontsize=fontsize)

hp.xticks(axes[1],coh2+[aux_1],['3.2','6.4','12.8','P1'],fontsize=fontsize)
hp.yticks(axes[1],[0.5,0.75,1],fontsize=fontsize)


aux_2=-0.7
i=0
axes[1].plot([-1.1,-.9],[PC_theory[i],PC_theory[i]],'-',color=colors[i])
i=1
axes[1].plot([-1.1+shift,-.9+shift],[PC_theory[i],PC_theory[i]],'-',color=colors[i])
i=2
axes[1].plot([-1.1,-.9],[PC_theory[i],PC_theory[i]],'-',color=colors[i])
axes[1].set_ylim([0.5,1])
hp.remove_axis(axes[2])
axes[1].set_xlabel('Second pulse coherence',fontsize=fontsize)
axes[1].set_ylabel('Accuracy',fontsize=fontsize)




####################Inset ###############
Pc_inset_y=[ PC_data[6],PC_data[7],PC_data[10] ]
Pc_inset_x=[ PC_data[8],PC_data[11],PC_data[12] ]
ax=fig.add_axes([0.67,0.79,0.06,0.16])
hp.remove_axis(ax)
ax.set_xlabel("coh1>coh2",fontsize=6)
ax.set_ylabel("coh2>coh1",fontsize=6)

ax.plot(Pc_inset_x,Pc_inset_y,'.k',markersize=markersize)
ax.plot([0.68,0.81],[0.68,0.81],'--k',lw=.5)
hp.xticks(ax,[])
hp.yticks(ax,[])


####################### Accuracy delay ####################

NTdelay=100
Tdelays=np.linspace(0,2500,NTdelay)
Tdelays= np.geomspace(100, 100000, NTdelay)
Tframe=120
Nc2=20

c2_delay=np.linspace(0.3,c2_fit,Nc2)
PC_delay=np.zeros((Nc2,NTdelay))
c4=1
stim=np.array([[12.8,3.2]])

c2_delay=np.array([c2_fit] )

i=0
labels=[r"$\alpha^*$"]
for ic2 in range(len(c2_delay)):
    coef_stim=[0,c2_delay[ic2],c4]
    coef_NO_stim=[0,c2_delay[ic2],c4]


    for it in range(NTdelay):
        PC_delay[ic2][it]=fdw.PR_DW_pulses_delay(coef_stim,coef_NO_stim,stim,Tframe,Tdelays[it],sigma_s,sigma_i,tau_fit,kmu_fit)
    
    axes[3].plot(Tdelays,PC_delay[ic2],'-',color="slategrey",label=labels[ic2])
    i=i+1




acc_PI_theory=np.zeros(NTdelay)
acc_PI=np.zeros(NTdelay)
mu=0.23
Tstim=120

dt=0.01
dt_sqrt=np.sqrt(dt)


mu1=12.8
mu2=3.8
Tstim=120/tau_fit

dt=0.01
dt_sqrt=np.sqrt(dt)

scale_k=0.44
acc_PI_theory=np.zeros(len(Tdelays))
acc_PI=np.zeros(len(Tdelays))
Ntrials=1000

NTstim=Tstim/dt

for idelay,delay in enumerate(Tdelays):
    delay=delay/tau_fit
    NTdelay=delay/dt
 
    loc=NTstim*dt*kmu_fit*scale_k*(mu1+mu2)
    scale=np.sqrt( 2*NTstim*((dt_sqrt*sigma_i)**2+(dt_sqrt*sigma_s)**2)+ int(NTdelay)*(sigma_i*dt_sqrt)**2 )
    acc_PI_theory[idelay]=1-norm.cdf(0,loc=loc,scale=scale)

axes[3].plot(Tdelays,acc_PI_theory,'k-',label="PI")
axes[3].legend(frameon=False,fontsize=fontsize)
axes[3].set_xscale('log')
axes[3].set_xlabel('Delay (ms)',fontsize=fontsize)
axes[3].set_ylabel('Accuracy',fontsize=fontsize)
hp.remove_axis(axes[3])
hp.yticks(axes[3],[0.5,0.62,0.75],fontsize=fontsize)
hp.xticks(axes[3],[100,1000,1e4,1e5],["$10^2$","$10^3$","$10^4$","$10^5$"],fontsize=fontsize)


fig.savefig(path+"figure7_python.svg")

plt.show()















