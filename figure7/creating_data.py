'''
This script creates a file with all the data from the two experiments.
The data points were extracted directly from kiani et al. 2013 and
Tohidi-Moghaddam et al. 2019

'''


import sys
sys.path.insert(0, '/home/genis/flexible-categorization/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt
import fit_dw_fc as fdw
from scipy.optimize import curve_fit


def psychometric(x,beta,b):
	return 1./(1.+np.exp(-beta*(x-b)))


#Kiani data

PC1_fit_kiani=np.array([0.566,0.646,0.784]) #fit
PC1_data=np.array([0.598,0.598,0.813])
lc_same=np.array([0.615,0.648,0.624,0.549])
mc_same=np.array([0.731,0.722,0.733,0.743])
hc_same=np.array([0.886,0.889,0.861,0.880])



PC2=np.array([ np.array([np.mean(lc_same),0.667,0.795]),np.array([0.693,np.mean(mc_same),0.828]),np.array([0.745,0.811,np.mean(hc_same)]) ] )
deltaPC=np.zeros(PC2.shape)
for i in range(len(PC1_data)):
	deltaPC[i]=PC2[i,:]-PC1_data[i]


MU=[ [3.2],[6.4],[12.8],[25.6],[51.2],[3.2,3.2],[3.2,6.4],[3.2,12.8],[6.4,3.2],[6.4,6.4],[6.4,12.8],[12.8,3.2],[12.8,6.4],[12.8,12.8] ]
PC_kiani=[ PC1_data[0],PC1_data[1],PC1_data[2],0.920,0.998,PC2[0][0],PC2[0][1],PC2[0][2],PC2[1][0],PC2[1][1],PC2[1][2],PC2[2][0],PC2[2][1],PC2[2][2] ]


Nblocks=11
Nsubjects=5
N_trials_block=np.array([7,7,7,7,7,28,28,28,28,28,28,28,28,28])
N=[]

for i in range(len(PC_kiani)):
	N.append(N_trials_block[i]*Nsubjects*Nblocks)

d=[]
for icoh in range(len(PC_kiani)):
	Ncorrect=int(round(PC_kiani[icoh]*N[icoh]))
	a=np.concatenate(( np.ones(Ncorrect),np.zeros(N[icoh]-Ncorrect)))
	d.append(a)

PC1_data_all=np.array([0.598,0.598,0.813,0.920,0.998])
x=np.array([3.2,6.4,12.8,25.6,51.2])
popt,_=curve_fit(psychometric,x,PC1_data_all,p0=[0,0])
x2=np.linspace(3.2,51.2,1000)
y2=psychometric(x2,popt[0],popt[1])

PC1_fit_kiani2=psychometric(x,popt[0],popt[1])

PC_kiani_fit=[ PC1_fit_kiani2[0],PC1_fit_kiani2[1],PC1_fit_kiani2[2],PC1_fit_kiani2[3],PC1_fit_kiani2[4],PC2[0][0],PC2[0][1],PC2[0][2],PC2[1][0],PC2[1][1],PC2[1][2],PC2[2][0],PC2[2][1],PC2[2][2] ]
d_kiani_fit=[]

for icoh in range(len(PC_kiani_fit)):
	Ncorrect=int(round(PC_kiani_fit[icoh]*N[icoh]))
	a=np.concatenate(( np.ones(Ncorrect),np.zeros(N[icoh]-Ncorrect)))
	d_kiani_fit.append(a)


#data Tohidi-Moghaddam
PC1_fit_Maryam=np.array([0.6,0.622,0.784]) #fit
PC1_data=np.array([0.622,0.695,0.775])

lc_same=np.array([0.666,0.579,0.660,0.549])
mc_same=np.array([0.731,0.779,0.785,0.773])
hc_same=np.array([0.897,0.890,0.889,0.856])

PC2=np.array([ np.array([np.mean(lc_same),0.731,0.835]),np.array([0.677,np.mean(mc_same),0.840]),np.array([0.754,0.798,np.mean(hc_same)]) ] )
deltaPC=np.zeros(PC2.shape)
for i in range(len(PC1_data)):
	deltaPC[i]=PC2[i,:]-PC1_data[i]


PC_Maryam=[ PC1_data[0],PC1_data[1],PC1_data[2],0.960,0.995,PC2[0][0],PC2[0][1],PC2[0][2],PC2[1][0],PC2[1][1],PC2[1][2],PC2[2][0],PC2[2][1],PC2[2][2] ]
Nblocks=11
Nsubjects=4
N_trials_block=np.array([7,7,7,7,7,28,28,28,28,28,28,28,28,28])
N2=[]

for i in range(len(PC_Maryam)):
	N2.append(N_trials_block[i]*Nsubjects*Nblocks)

for icoh in range(len(PC_Maryam)):
	Ncorrect=int(round(PC_Maryam[icoh]*N2[icoh]))
	a=np.concatenate(( np.ones(Ncorrect),np.zeros(N2[icoh]-Ncorrect)))
	d[icoh]=np.concatenate((d[icoh],a))


PC1_data_all=np.array([0.622,0.695,0.775,0.968,0.995])
x=np.array([3.2,6.4,12.8,25.6,51.2])
popt,_=curve_fit(psychometric,x,PC1_data_all,p0=[0,0])
x2=np.linspace(3.2,51.2,1000)
y2=psychometric(x2,popt[0],popt[1])

PC1_fit_Maryam2=psychometric(x,popt[0],popt[1])


PC_Maryam_fit=[ PC1_fit_Maryam2[0],PC1_fit_Maryam2[1],PC1_fit_Maryam2[2],PC1_fit_Maryam2[3],PC1_fit_Maryam2[4],PC2[0][0],PC2[0][1],PC2[0][2],PC2[1][0],PC2[1][1],PC2[1][2],PC2[2][0],PC2[2][1],PC2[2][2] ]
d_Maryam_fit=[]

for icoh in range(len(PC_Maryam_fit)):
	Ncorrect=int(round(PC_Maryam_fit[icoh]*N[icoh]))
	a=np.concatenate(( np.ones(Ncorrect),np.zeros(N[icoh]-Ncorrect)))
	d_Maryam_fit.append(a)

PC=np.zeros(len(d))
error_PC=np.zeros(len(d))
for icoh in range(len(PC)):
	PC[icoh],error_PC[icoh]=ba.mean_and_error(d[icoh])

Nfinal=np.array(N)+np.array(N2)


###### fit 1 pulse data for two experiments ######
y=PC[:5]
x=np.array([3.2,6.4,12.8,25.6,51.2])
popt,_=curve_fit(psychometric,x,y,p0=[0,0])
x2=np.linspace(3.2,51.2,1000)
y2=psychometric(x2,popt[0],popt[1])

plt.figure()
plt.plot(x,y,'o')
plt.plot(x2,y2,'-')
PC1_fit=psychometric(x,popt[0],popt[1])
plt.plot(x,PC1_fit,'o')

Ncor=[]
Ninc=[]
for itrial in range(len(d)):
	Ncor.append(  np.sum(d[itrial])   )
	Ninc.append(    len(d[itrial])-Ncor[-1] )


data={'coh': [ [3.2], [6.4],[12.8],[25.6],[51.2], [3.2,3.2],[3.2,6.4],[3.2,12.8],[6.4,3.2],[6.4,6.4],[6.4,12.8],[12.8,3.2],[12.8,6.4],[12.8,12.8]],
'd':d,'PC':PC,'PC_error':error_PC,'N':Nfinal,'PC1_fit_kiani':PC1_fit_kiani,
'PC1_fit_Maryam':PC1_fit_Maryam,'PC_kiani':PC_kiani,'PC_Maryam':PC_Maryam,
'PC1_fit':PC1_fit,"PC_kiani_fit":PC_kiani_fit,"PC_Maryam_fit":PC_Maryam_fit,
"d_maryam_fit":d_Maryam_fit,"d_kiani_fit":d_kiani_fit,"Ncor":Ncor,"Ninc":Ninc
}




for i in range(len(data['coh'])):
	data['coh'][i]=np.array(data['coh'][i])


path='/home/genis/flexible-categorization/figure7/'

f=open(path+'data_kiani_Maryam.pickle','wb')
pickle.dump(data,f)
f.close()


Ncor=[]
Ninc=[]
for itrial in range(len(data['coh'])):
	Ncor.append(  np.sum(d_kiani_fit[itrial])   )
	Ninc.append(    len(d_kiani_fit[itrial])-Ncor[-1] )



data2={'coh':[ [3.2], [6.4],[12.8],[25.6],[51.2], [3.2,3.2],[3.2,6.4],[3.2,12.8],[6.4,3.2],[6.4,6.4],[6.4,12.8],[12.8,3.2],[12.8,6.4],[12.8,12.8]],
		'Ncor':Ncor,'Ninc':Ninc
}


import pandas as pd

df=pd.DataFrame.from_dict(data2)
pd.DataFrame.to_json(df,path+'data_kiani.json')
pd.DataFrame.to_csv(df,path+'data_kiani.csv')








