
"""
@author: genis
This script computes the maxium level of internal noise 
without transition (<1%) during the delay
"""

import sys
sys.path.insert(0, '/home/genis/flexible-categorization/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt
import fit_dw_fc as fdw
from scipy.optimize import minimize


path='/home/genis/flexible-categorization/figure7/'
f=of=open(path+'optimizePC_parameter_fdw.pickle','rb')
data_save=pickle.load(f,encoding='latin1')
f.close()
ibest=data_save['ibest']
ibest=3907
param=data_save['final_res'][ibest].x
PC_theory=data_save['final_PC_fit'][ibest]

kmu_fit=param[0]
c2_fit=param[1]
sigma_fit=param[2]
tau_fit=param[3]
coef=np.array([0.,c2_fit,1.,0.])
Nsigma=100
sigmas_theory=np.linspace(0.05, sigma_fit,Nsigma)
PRL=np.zeros(Nsigma)
T=1000.
roots=fdw.roots_DW(coef)
for isigma,sigma in enumerate(sigmas_theory): 
    _,PRL[isigma]=fdw.Transition_probabilites(coef,roots[0],roots[1],roots[2],sigma,T/tau_fit)


plt.figure()
plt.plot(sigmas_theory,PRL,'-')

plt.show()

ibest=np.where(np.abs(PRL)<0.01)[0][-1]
sigma_i=sigmas_theory[ibest]
sigma_s=np.sqrt(sigma_fit**2-sigma_i**2)
print(sigma_i/sigma_fit,PRL[ibest],sigma_s)


f=open(path+'internal_noise_fdw.pickle','wb')
data_save={'internal_noise':sigma_i,'stimulus_noise':sigma_s,'noise': sigmas_theory[ibest]}
data_save=pickle.dump(data_save,f)
f.close()

