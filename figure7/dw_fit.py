
"""
@author: genis
This script fits the DW to the data from Kiani et al. 2013 and
Tohidi-Moghaddam et al. 2019
"""
import sys
sys.path.insert(0, '/home/genis/flexible-categorization/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt
import fit_dw_fc as fdw
from scipy.optimize import minimize


path='/home/genis/flexible-categorization/figure7/'
f=open(path+'data_kiani_Maryam.pickle','rb')
data=pickle.load(f)
f.close()


Tframe=120
k=0.0125
c2=0.75
c4=1
sigma=0.3
data2={'coh':data['coh'],'d':data['d_kiani_fit']}


args=[data2,Tframe]
Ncondition=1000
final_res=[]
final_PC_fit=[]
initial_param=[]
j=0


for icondition in range(Ncondition):
	print("icondition",icondition)
	
	k=0.1*np.random.random()
	c2=1*np.random.random()
	c4=1*np.random.random()
	sigma=1*np.random.random()
	tau=10+10*np.random.random()
	param=[k,c2,sigma,tau]
	initial_param.append(param)

	res=minimize(fdw.kiani_fit,param,args=args, method='nelder-mead')
	kf=res.x[0]
	c2f=res.x[1]
	c4=1
	sigmaf=res.x[2]
	tauf=res.x[3]


	PC_fit=fdw.PR_DW_diff_number_frames([0,c2f,c4],data['coh'],Tframe,sigmaf,tauf,kf)
	final_res.append(res)
	final_PC_fit.append(PC_fit)
	if(final_res[icondition].fun<final_res[j].fun):
		j=icondition





data_save={'final_res':final_res,'final_PC_fit':final_PC_fit,'ibest':j}

# path='/home/genis/Dropbox/paper/kiani2013/'
#f=open(path+'optimizePC_parameter_fdw_kiani_fit_2p.pickle','wb')
f=open(path+'optimizePC_parameter_fdw_kiani_fit.pickle','wb')
pickle.dump(data_save,f)
f.close()


###using synthetic data ###################
# param=[k,c2,c4,sigma]
# args=[data]
# PC_DW=ba.kiani_PC(param,args)
# dataDW=data.copy()
# dataDW['PC']=PC_DW

# # res=minimize(ba.kiani_fit,param,args=args, method='L-BFGS-B',bounds=[[0.1,1000],[0.01,10000],[0.01,10000],[0.01,10000] ])


# k=0.03
# c2=0.5
# c4=0.5
# sigma=0.5



# param=[k,c2,c4,sigma]
# args=[dataDW]


# res=minimize(ba.kiani_fit,param,args=args, method='nelder-mead')

# PC_fit=ba.kiani_PC(res.x,args)


# k=0.1
# c2=1
# c4=0.4
# sigma=0.5
# args=[dataDW]

# a=ba.kiani_fit(param,args)


# res=minimize(ba.kiani_fit,param,args=args, method='L-BFGS-B',bounds=[[0.1,1000],[0.01,10000],[0.01,10000],[0.01,10000] ])

# params=res.x
# k=res.x[0]
# c2=res.x[1]
# c4=res.x[2]
# sigma=res.x[3]

# PC1_DW=np.zeros(5)
# for i in range
# plt.figure()
# im3=plt.imshow(deltaPC,extent=[MU[0],MU[-1],MU[0],MU[-1]],origin='lower',aspect=1.4,cmap='bwr',vmax=0.2,vmin=-0.2)
# plt.colorbar(im3)
# plt.show()