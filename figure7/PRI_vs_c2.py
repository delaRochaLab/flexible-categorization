"""
@author: genis
This script computes the Primacy-Recency Index of the Double Well model
as a function of c2
"""
import sys
sys.path.insert(0, '/home/genis/flexible-categorization/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt
import fit_dw_fc as fdw
from scipy.optimize import minimize


path='/home/genis/flexible-categorization/figure7/'

######################### ACCURACY 2 pulse same mu ###################################

mu_small=0.04
mu_medium=0.08
mu_high=0.16

f=open(path+'optimizePC_parameter_fdw.pickle','rb')
data_save=pickle.load(f,encoding='latin1')
f.close()

ibest=data_save['ibest']
ibest=3907
param=data_save['final_res'][ibest].x
PC_theory=data_save['final_PC_fit'][ibest]

mu_small_logit=3.2
mu_medium_logit=6.4
mu_high_logit=12.8
MU=np.array([ [3.2,3.2],[3.2,6.4],[3.2,12.8],[6.4,3.2],[6.4,6.4],[6.4,12.8],[12.8,3.2],[12.8,6.4],[12.8,12.8] ])


kmu_fit=param[0]
c2_fit=param[1]
c4=1
Tframe=120
sigma_fit=param[2]
tau_fit=param[3]



f=open(path+'data_kiani_Maryam.pickle','rb')
data_data=pickle.load(f)
f.close()
args_data=[data_data]


plt.figure()
plt.plot(data_data['PC'],PC_theory,'o')
plt.plot([0.5,1],[0.5,1],'-')




plt.show()


Nc2=51
c2range=1.
C2=np.linspace(c2_fit-c2range,c2_fit+c2range,Nc2)
PRI=np.zeros(Nc2)
Ntrial=10000
coef=[0,c2_fit,c4]
PC=fdw.PR_DW_frames(coef,MU,Tframe,sigma_fit,tau_fit,kmu_fit)
PRI_fit=ba.PRI_with_frames(PC,MU,np.ones(len(PC),dtype=int)*Ntrial)
plt.plot(data_data['PC'][5:],PC,'o')



for ic2,c2 in enumerate(C2):
    coef=[0,c2,c4]
    PC=fdw.PR_DW_frames(coef,MU,Tframe,sigma_fit,tau_fit,kmu_fit)
    PRI[ic2]=ba.PRI_with_frames(PC,MU,np.ones(len(PC),dtype=int)*Ntrial)



plt.figure()
plt.plot(C2,PRI)
plt.show()

f=open(path+'PRI_c2.pickle','wb')
data_save={'PRI':PRI,'C2':C2,'c2_fit':c2_fit,'PRI_fit':PRI_fit}
pickle.dump(data_save,f)
f.close()





