"""
@author: genis
This script computes the Primacy-Recency Index of the human subjects from
kiani et al. 2013 andTohidi-Moghaddam et al. 2019
"""

import sys
sys.path.insert(0, '/home/genis/flexible-categorization/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt
import fit_dw_fc as fdw
from scipy.optimize import minimize


path='/home/genis/flexible-categorization/figure7/'


f=open(path+'data_kiani_Maryam.pickle','rb')
data=pickle.load(f)
f.close()

PRI,conf_interval,fit2pulse=ba.PRI_with_frames(data['PC'][5:],data['coh'][5:],data['N'][5:],True)

beta2pulse=fit2pulse.params
beta2pulse_stderr=fit2pulse.bse
pvalues_2pulse=fit2pulse.pvalues

save={'PRI':PRI,'conf_interval':conf_interval,'beta2pulse':beta2pulse,'beta2pulse_stderr':beta2pulse_stderr,'pvalues_2pulse':pvalues_2pulse}
f=open(path+'PRI_data.pickle','wb')
pickle.dump(save,f)
f.close()





