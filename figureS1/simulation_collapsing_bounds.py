"""
Created on Tue Jan 23 10:51:54 2018

@author: genis

This script computes the psychophysical kernels for the the DDM with absorbing bounds

"""
import sys
sys.path.insert(0, '/home/genis/flexible-categorization/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import pickle
import matplotlib.pyplot as plt




Nsigmas=30
T=1000
tau=200
dt=tau/40.
NT=int(T/dt)

sigma_i=0.1
mu=0.
x0=0.




################ Traces Collapsing bounds ##################

Nstim=30
Ntraces=10
sigma_s=0.3
stim=sigma_s*np.random.randn(Nstim,NT) 
internal_noise=sigma_i*np.random.randn(Nstim,NT) 
mu=0.0
#urgency=10
#urgency=200
urgency=50
Bi=0.5

time=np.linspace(0,NT-1,NT)
a=-Bi/(1-np.exp(time[-1]/urgency))
bound=Bi+a*(1-np.exp(time/urgency))

d,xfinal=sd.simulation_DDMCB_traces(mu,stim,internal_noise,bound,T,tau,x0)
xtrace=np.array(xfinal)
plt.figure()

plt.plot(time,bound,'k--')
plt.plot(time,-bound,'k--')

for iplot in range(10):
    plt.plot(time,xtrace[iplot][1:])

plt.show()


################ PK ##################
Nboot=500
Nsigmas=30
sigmas=np.linspace(0.0025,0.8,Nsigmas)
Nstim=int(1e4)

#sigmas=[0.005,0.1,0.5,0.75]

KERNEL=[]
PRR=np.zeros(len(sigmas))
NPKA=np.zeros(len(sigmas))

for isigma in range(len(sigmas)):
    print("isigma", isigma)
    stim=sigmas[isigma]*np.random.randn(Nstim,NT) #stimulus #I add minus here because the equation is -
    internal_noise=sigma_i*np.random.randn(Nstim,NT) #internal nopise
    d,xfinal=sd.simulation_DDMCB(mu,stim,internal_noise,bound,T,tau,x0)
    d=np.array(d)

    a=ba.kernel_err(stim,d,error=False)
    KERNEL.append(a["kernel"])
    PRR[isigma]=ba.PK_slope(a["kernel"])
    NPKA[isigma]=ba.total_area_kernel_PInormalize(a["kernel"])

    

isigmas_plot=[25,10,5,2]
plt.figure()
for isigma in isigmas_plot:
    plt.plot(KERNEL[isigma])


Nstim=30
xtraces=np.zeros((len(sigmas),Nstim,NT+1))
for isigma,sigma_s in enumerate(sigmas):
    stim=sigma_s*np.random.randn(Nstim,NT) #stimulus #I add minus here because the equation is -
    internal_noise=sigma_i*np.random.randn(Nstim,NT) #internal nopise

    d,xfinal=sd.simulation_DDMCB_traces(mu,stim,internal_noise,bound,T,tau,x0)
    xtraces[isigma]=np.array(xfinal)






data_to_save={}
data_to_save['kernel']=KERNEL
data_to_save['PRI']=PRR
data_to_save['NPKA']=NPKA
data_to_save['sigmas']=sigmas
data_to_save['sigma_i']=sigma_i
data_to_save['tau']=tau
data_to_save['T']=T
data_to_save["bound"]=bound
data_to_save["x0"]=x0
data_to_save["xtraces"]=xtraces

path="/home/genis/flexible-categorization/figureS1/"
f=open(path+'kernels_primacy_recency_DDMCB_urgency'+str(urgency)+'.pickle','wb')
pickle.dump(data_to_save,f)
f.close()


















