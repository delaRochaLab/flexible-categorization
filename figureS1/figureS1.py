#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 10:51:54 2018

@author: genis
"""

# import sys
# sys.path.insert(0, '/home/gprat/Dropbox/python_functions')
# sys.path.insert(0, '/home/genis/Dropbox/python_functions')

# import behaviour_analysis as ba
# import behaviour_plot as bp
# import numpy as np
# import simulation_diffusion_paper as sd
# import matplotlib.pyplot as plt
# import matplotlib.colors as colors
# import matplotlib as mpl
# import matplotlib.cm as cmx	
# import help_plot as hp 
# import pickle
# reload(sd)
# reload(ba)
# reload(bp)


import sys
sys.path.insert(0, '/home/genis/flexible-categorization/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import pickle
import matplotlib.pyplot as plt
import help_plot as hp


plt.rcParams['svg.fonttype'] = "none"
plt.rcParams['pdf.fonttype'] = 'type3'
######################Colors#############
color_sigma=[ "#560105","#8B1518","#C1332D","#F55546"]
color_sigma=[ "#560105","#C1332D","#F55546"]


color_PI="black"
color_DDMA="darkcyan"
color_DDMR="silver"

#######################

w=0.3
h=0.225
h0=0.15
hf=0.05
w0=0.15
wf=0.05
ws=(1-2*w-w0-wf)
hs=(1-3*h-h0-hf)/2.

hinset=0.055
#f=open('kernels.pickle','rb')
#data_to_save=pickle.load(f)
#f.close()
#kernel=data_to_save['kernel']
##data_to_save['coef_PI']=coef_PI
##data_to_save['coef_DW']=coef_DW
#data_to_save['coef_recency']=coef_recency
fontsize=8
linewidth=2           

#isigmas=[[0,10,15,23],[0,10,19,23],[0,10,19,23]]


#c=['darkslategray','black','darkorange']
#c=['darkcyan','black','darkred','grey']
#colors=[scalarMap.to_rgba(0),scalarMap.to_rgba(3)]

YLIM=[(0.5,0.54),(0.5,0.58),(0.5,0.58)]
fig, axes = plt.subplots(nrows=4, ncols=4,figsize=(14/2.54,14/2.54))

#AXES=[]

#h0=0.1


############# PLOOOT KERNELS ############


#isigmas=[[25,10,5,1],[25,10,5,1],[25,10,5,1]]
isigmas=[[25,10,1],[25,10,1],[25,10,1]]

#isigmas=[[2,5,10,25],[2,5,10,25],[2,5,10,25]]
#isigmas=[[4,3,2,0],[4,3,2,0],[4,3,2,0]]

max_plot=3
URGENCY=[10,50,200]
for iurgency,urgency in enumerate(URGENCY):
    path="/home/genis/flexible-categorization/figureS1/kernels_primacy_recency_DDMCB_urgency"+str(urgency)+".pickle"
    
    f=open(path,"rb")
    data=pickle.load(f)
    kernel=data['kernel']
    NPKA=data["NPKA"]
    PRI=data["PRI"]

    for icolor,isigma in enumerate(isigmas[iurgency]):
        icol=3-icolor
     
        kernel_plot=kernel[isigma]
        t=np.linspace(0,1,len(kernel_plot))
        hp.kernel_plot(axes[iurgency][max_plot-icolor],kernel_plot,1.,color=color_sigma[2-icolor],smoothing_window=5,linewidth=linewidth,yticks=[0.5,0.55,0.6],yticklabels=["",""],ylim=0.6,fontsize=fontsize)
        hp.remove_axis(axes[iurgency][max_plot-icolor])
        axes[iurgency][max_plot-icolor].set_xlabel('')
        axes[iurgency][max_plot-icolor].set_ylabel('')
        axes[iurgency][max_plot-icolor].set_ylabel('')
        hp.remove_axis_bottom(axes[iurgency][2-icolor+1])
            
        if 2-icolor+1 > 1:
            axes[iurgency][max_plot-icolor].spines['left'].set_visible(False)
            axes[iurgency][max_plot-icolor].set_yticks([])
    axes[1][1].set_ylabel('Stimulus Impact',fontsize=fontsize)
    axes[iurgency][1].set_yticklabels(['0.5','0.55','0.6'],fontsize=fontsize)
    


############# PLOOOT TRACES ############


# Nstim=10
# T=data["T"]
# tau=data["tau"]
# sigma_i=data["sigma_i"]
# sigmas=data["sigmas"]
# x0=data["x0"]
# bound=data["bound"]
# dt=tau/40.
# NT=int(T/dt)

#ls=4
#hs=8
#indices=[[0,1],[3,2],[0,0]]
#indices=[[ls,hs],[ls,hs],[ls,hs]]
indices_trace=[0,0]
indices_sigma=[1,10]

# indices=[0,1]


i_colors=[0,1]
# isigmas_plot=[0,3]

for iurgency,urgency in enumerate(URGENCY):
    path="/home/genis/flexible-categorization/figureS1/kernels_primacy_recency_DDMCB_urgency"+str(urgency)+".pickle"
    
    f=open(path,"rb")


    data=pickle.load(f)
    xtraces=data["xtraces"]
    bound=data["bound"]

    x_time=np.linspace(0,data["T"],len(xtraces[0][0]))
    x_time_bound=np.linspace(0,data["T"],len(bound))

    axes[iurgency][0].axhline(0,0,1,color='grey',linestyle='dashed',linewidth=0.5)
    axes[iurgency][0].spines['left'].set_visible(False)
    for icol,isigma in enumerate(indices_sigma):
        
#        internal_noise=np.random.randn(Nstim,T)
  
        
        #axes[imodel][0].plot(x_traces[indices[imodel][icol]],x_time,'-',linewidth=1,color=color_sigma[2-i_colors[icol]])
        axes[iurgency][0].plot(x_time,xtraces[isigma][indices_trace[icol]],'-',linewidth=1,color=color_sigma[i_colors[icol]])
        axes[iurgency][0].plot(x_time_bound,bound,'k--',linewidth=.5)
        axes[iurgency][0].plot(x_time_bound,-bound,'k--',linewidth=.5)


        hp.remove_axis(axes[iurgency][0])

axes[0][0].set_ylabel('')
axes[2][0].set_ylabel('')

lw_traces=0.5
axes[0][0].set_yticklabels(['0'],fontsize=fontsize)
axes[1][0].set_yticklabels(['','0',''],fontsize=fontsize)
axes[2][0].set_yticklabels(['','0',''],fontsize=fontsize)
# axes[1][0].axhline(0.5,0,x_time[-1],color='grey',linestyle='dashed',linewidth=lw_traces)
# axes[1][0].axhline(-0.5,0,x_time[-1],color='grey',linestyle='dashed',linewidth=lw_traces)
# axes[0][0].axhline(0.5,0,x_time[-1],color='grey',linestyle='dashed',linewidth=lw_traces)
# axes[0][0].axhline(-0.5,0,x_time[-1],color='grey',linestyle='dashed',linewidth=lw_traces)
axes[0][0].set_yticks([0])
axes[1][0].set_yticks([0])
axes[2][0].set_yticks([0])
axes[1][0].set_yticklabels(['0'],fontsize=fontsize)
axes[2][0].set_yticklabels(['0'],fontsize=fontsize)

axes[1][0].set_ylim(-0.6,0.6)
axes[0][0].set_ylim(-0.6,0.6)




##### xticks #########
axes[0][0].set_xticks([0,0.5,1])
axes[1][0].set_xticks([0,0.5,1])
axes[2][0].set_xticks([0,0.5,1])

axes[0][0].set_xticklabels(['','',''],fontsize=fontsize)
axes[2][0].set_xticklabels(['','',''],fontsize=fontsize)
axes[1][0].set_xticklabels(['','',''],fontsize=fontsize)
#axes[2][0].set_xlabel('Time (s)',fontsize=fontsize)

axes[1][0].set_ylabel('Decision variable x',fontsize=fontsize)
hp.remove_axis_bottom(axes[2][0])

axes[1][0].tick_params(axis='x',which='both',bottom='off',top='off',labelbottom='off')
axes[0][0].spines['bottom'].set_visible(False)
axes[0][0].tick_params(axis='x',which='both',bottom='off',top='off',labelbottom='off')
axes[1][0].spines['bottom'].set_visible(False)



#







# ### Primancy vs recency ratio  and total area####

c=[color_PI,color_DDMA,color_DDMR]
color_urgency=[  "#4D3F07","#BB9D2C","#F8D046"]

#isigmas=[[0,10,3,29],[0,10,19,29],[0,10,19,29]]


w0=0.1
wf=0.025
ws=0.15
w=(1-ws-w0-wf)/2
ax_area=fig.add_axes([0.1,0.09,w,0.22])
ax_pri=fig.add_axes([0.6,0.09,w,0.22])


markersize=2
for iurgency,urgency in enumerate(URGENCY):



    path="/home/genis/flexible-categorization/figureS1/kernels_primacy_recency_DDMCB_urgency"+str(urgency)+".pickle"
    
    f=open(path,"rb")


    data=pickle.load(f)
    xtraces=data["xtraces"]
    bound=data["bound"]
    PRI=data["PRI"]
    NPKA=data["NPKA"]
    sigmas=data["sigmas"]

    f.close()


    ax_area.plot(sigmas,NPKA,'-',color=color_urgency[iurgency],markersize=markersize,linewidth=linewidth)         
    sigma_i=data["sigma_i"]

ax_area.plot([sigma_i,sigma_i],[0,0.05],color='k')
ls='solid'       

for iurgency,urgency in enumerate(URGENCY):
    path="/home/genis/flexible-categorization/figureS1/kernels_primacy_recency_DDMCB_urgency"+str(urgency)+".pickle"
    

    f=open(path,"rb")
    data=pickle.load(f)
    xtraces=data["xtraces"]
    bound=data["bound"]
    PRI=data["PRI"]
    NPKA=data["NPKA"]
    sigmas=data["sigmas"]

    f.close()



    ax_pri.plot(sigmas[1:],PRI[1:],'-',color=color_urgency[iurgency],markersize=markersize,linewidth=linewidth,label=r"$\tau_u$="+str(URGENCY[iurgency]))         
ax_pri.legend(frameon=False,fontsize=fontsize)


ax_pri.set_ylim([0,1])


ax_pri.plot([sigma_i,sigma_i],[-1,-0.95],color='k')

        
ax_pri.set_xlabel('Stimulus fluctuations '+r'$\sigma_s$',fontsize=fontsize)
ax_area.set_xlabel('Stimulus fluctuations '+r'$\sigma_s$',fontsize=fontsize)

ax_area.set_ylabel(r'Normalized PK area',fontsize=fontsize)
ax_pri.set_ylabel(r'Normalized PK slope',fontsize=fontsize)      

hp.remove_axis(ax_area)
hp.remove_axis(ax_pri)


hp.yticks(ax_area,[0,0.5,1],yticklabels=['0','0.5','1'],fontsize=fontsize)




hp.yticks(ax_pri,[-1,0,1],fontsize=fontsize)



hp.xticks(ax_area,[0,0.3,0.6],fontsize=fontsize)
hp.xticks(ax_pri,[0,0.3,0.6],fontsize=fontsize)






#w_traces=0.18
#w=0.18
h=0.13
h0=0.1
hf=0.05
w0=0.1
wf=0.025
#ws0=0.12
ws_kernel=0.01
ws0=0.15
w=(1-ws0-w0-wf-2*ws_kernel)/4.
w_traces=w
hs=(0.8-3*h-hf-h0)/3.

for iax in range(len(axes)-1):
    axes[iax][0].set_position([w0,1-h*(1+iax)-hs*(iax)-hf ,w_traces, h])

    for iax2 in range(1,len(axes[0])):
        axes[iax][iax2].set_position([w0+w_traces+ws0+(iax2-1)*(w+ws_kernel),   1-h*(1+iax)-hs*(iax)-hf ,w, h])
    

# fontsize_legend=15
# fig.text(0.005,0.95,'a',fontsize=fontsize_legend)
# fig.text(0.005+ws0+w,0.95,'b',fontsize=fontsize_legend)

# fig.text(0.005,0.34,'c',fontsize=fontsize_legend)
# fig.text(0.45,0.34,'d',fontsize=fontsize_legend)





########## ading potentials ##############
x_pot=w0+w+ws0+w/2-0.05
y_pot=[0.88,0.67,0.44]
fontsize2=8
fig.text(0.07,0.97,r"$\tau_u$="+str(URGENCY[0]),fontsize=fontsize2)
fig.text(0.07,0.76,r"$\tau_u$="+str(URGENCY[1]),fontsize=fontsize2)
fig.text(0.07,0.54,r"$\tau_u$="+str(URGENCY[2]),fontsize=fontsize2)



# URGENCY=[10,10,10]
# color_pot='dimgrey'
# for i in range(len(URGENCY)):
#     ax_potential=fig.add_axes([x_pot,y_pot[i],0.1,0.1])
#     hp.plot_potential(ax_potential,model[i],xmin=-1.5,xmax=1.5,ymin=-0.4,ymax=0.4,axis_off=True,color_left=color_pot,color_right=color_pot,coef=[0.05],linewidth=2)

# x_pot=0.2
# y_pot=[0.17,0.14,0.08]
# URGENCY=[10,10,10]

# for i in range(len(URGENCY)):
#     ax_potential=fig.add_axes([x_pot,y_pot[i],0.07,0.07])
#     hp.plot_potential(ax_potential,model[i],xmin=-1.5,xmax=1.5,ymin=-0.4,ymax=0.4,axis_off=True,color_left=c[i],color_right=c[i],coef=[0],linewidth=2)





  
#    
axes[3][0].axis('off')
axes[3][1].axis('off')
axes[3][2].axis('off')
axes[3][3].axis('off')
#axes[3][4].axis('off')



plt.show()



#plt.tight_layout()     
#fig.savefig('/home/genis/Dropbox/paper/figure1/figure1.pdf')
#fig.savefig('/home/genis/Dropbox/paper/figure1/figure1.png')
fig.savefig('/home/genis/flexible-categorization/figureS1/figureS1_python.svg')






 
            




















