"""
@author: genis
This script computes the accuracy of the Double Well
model for different values of stimulus fluctuations magnitude,
internal noise and stimulus durations.
"""
import sys
sys.path.insert(0, '/home/genis/flexible-categorization/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt
import fit_dw_fc as fdw
from importlib import reload  

reload(ba)
reload(fdw)
PK=[]
NPKA=[]
PRI=[]

Nstim=int(1e5)
Nsigmas=50
T=2000
tau=200.
dt=tau/40.

NT=int(T/dt)
time=np.linspace(0,T,NT)

sigma_i=0.0
c2=2
c4=4
SIGMAS_S=np.linspace(0.05,1.0,Nsigmas)


# #############      KERNEL sigmas        ######################
for isigma in range(len(SIGMAS_S)):
    stim_noise=SIGMAS_S[isigma]*np.random.randn(Nstim,NT)
    internal_noise=sigma_i*np.random.randn(Nstim,NT) #internal nopise
    coef=np.array([0.0,c2,c4])
    d,xfinal=sd.simulation_DW(coef,stim_noise,internal_noise,T,tau,0.)
    d=np.array(d)
    print('computing kernel DW',isigma)
    kernel=ba.kernel_err(stim_noise,d,error=False)
    PK.append(kernel['kernel'])
    PRI.append(ba.PK_slope(PK[-1]) )
    NPKA.append(ba.total_area_kernel_PInormalize(PK[-1]))

plt.figure()
for isigma in range(len(SIGMAS_S)):
    plt.plot(time,PK[isigma],color=hp.colormap('hot',isigma,0,len(SIGMAS_S)+2))


###### Performance sigma #####

Performance=np.zeros(Nsigmas)
Performance_err=np.zeros(Nsigmas)
mu=0.15
coef=np.array([mu,c2,c4])
for isigma in range(len(SIGMAS_S)):
    print("Computing accuracy", isigma)
    stim_noise=SIGMAS_S[isigma]*np.random.randn(Nstim,NT)
    internal_noise=sigma_i*np.random.randn(Nstim,NT) #internal nopise
    d,xfinal=sd.simulation_DW(coef,stim_noise,internal_noise,T,tau,0.)
    d=np.array(d)
    Performance[isigma],Performance_err[isigma]=ba.mean_and_error((d+1)/2)

Performance_theory,_,prr,prl,_=fdw.Performance_sigma_tau(coef,T,SIGMAS_S,tau,sigma_i=sigma_i)

plt.figure()
plt.plot(SIGMAS_S,Performance,'o-')
plt.plot(SIGMAS_S,Performance_theory,'-')
plt.show()


###### Performance sigma, T #####
#Nsigmas=1000
#SIGMAS_S=np.linspace(0.05,1.0,Nsigmas)
TS=[100,200,300,400,500,600,700,800,900,1000,2000,3000,4000,5000,6000,8000,10000]
Performance_theory_T=np.zeros((len(TS),Nsigmas))
_,Performance_t0,_,_,Performance_ts=fdw.Performance_sigma_tau(coef,T,SIGMAS_S,tau,sigma_i=sigma_i)
for it in range(len(TS)):
    Performance_theory_T[it],_,_,_,_=fdw.Performance_sigma_tau(coef,TS[it],SIGMAS_S,tau,sigma_i=sigma_i)

plt.figure()
plt.plot(SIGMAS_S,Performance_t0,'-')
plt.plot(SIGMAS_S,Performance_ts,'-')
for it in range(len(TS)):
    plt.plot(SIGMAS_S,Performance_theory_T[it],'-')


    
    
plt.show()


###### Performance sigma, sigma_i #####


SIGMAS_I=[0.1,0.3,0.5]
Performance_theory_sigma_i=np.zeros((len(SIGMAS_I),Nsigmas))
for isigma_i in range(len(SIGMAS_I)):
    Performance_theory_sigma_i[isigma_i],_,_,_,_=fdw.Performance_sigma_tau(coef,T,SIGMAS_S,tau,sigma_i=SIGMAS_I[isigma_i])  

plt.figure()
for isigma_i in range(len(SIGMAS_I)):
    plt.plot(SIGMAS_S,Performance_theory_sigma_i[isigma_i],'-')
    
plt.show()




path="/home/genis/flexible-categorization/figure3/"
data={ "PK":PK,"PRI":PRI,"NPKA":NPKA,"SIGMAS_S":SIGMAS_S,"T":T,
"sigma_i":sigma_i,"c2":c2,"c4":c4,"tau":tau,"mu":mu,
"Performance":Performance,"Performance_err":Performance_err,
"Performance_theory":Performance_theory,
"Performance_t0":Performance_t0,"Performance_ts":Performance_ts,
"Performance_theory_T":Performance_theory_T,"TS":TS,
"Performance_theory_sigma_i":Performance_theory_sigma_i,"SIGMAS_I":SIGMAS_I,
"prr":prr,"prl":prl
}


f=open(path+"pk_accuracy_sigmas.pickle","wb")
pickle.dump(data,f)
f.close()

