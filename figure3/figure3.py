#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 10:51:54 2018

@author: genis
"""
import sys
sys.path.insert(0, '/home/genis/flexible-categorization/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import fit_dw_fc as fdw


#rc('text',usetex=False)


fontsize=8

plt.rcParams['svg.fonttype'] = "none"
plt.rcParams['pdf.fonttype'] = 'type3'

######################Colors#############
color_sigma=[ "#560105","#8B1518","#C1332D","#F55546"]
color_sigma_i=[  "#4D3F07","#826C17","#BB9D2C","#F8D046"]
color_T=["#262932","#41485A","#5B6A88","#748DBD","#89B3F8"]
color_PI="black"
color_DDMA="darkcyan"
color_DDMR="silver"
##########################################

path="/home/genis/flexible-categorization/figure3/"


fig = plt.figure(figsize=(12./2.54,18./2.54))

###### 3 d psychometric curve ####
Nsigmas=50
T=2000
tau=200.
dt=tau/40.
MU=np.linspace(-0.4,.4,Nsigmas)
sigma_i=0.0
c2=2
c4=4


ax = fig.add_subplot(421, projection="3d")

ax.xaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
ax.yaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
ax.zaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
SIGMAS_S=np.linspace(0.05,1.0,Nsigmas)
MU=np.linspace(-0.4,.4,Nsigmas)



Performance_theory=np.zeros((len(SIGMAS_S),len(MU)))
MU_plot, SIGMAS_S_plot = np.meshgrid(MU, SIGMAS_S)
for imu,mu in enumerate(MU):
    coef=np.array([mu,c2,c4])
    Performance_theory[:,imu],_,prr,prl,_=fdw.Performance_sigma_tau(coef,T,SIGMAS_S,tau,sigma_i=sigma_i)


ax.plot_surface(MU_plot,SIGMAS_S_plot,Performance_theory,color="white",linewidth=0,antialiased=False,alpha=0)

#### grid surface ###
color_grid="black"
lw=0.4

ISIGMAS=range(0,50,2)
for icolor,isigma in enumerate(ISIGMAS):
    ax.plot(MU,SIGMAS_S[isigma]*np.ones(len(MU)),Performance_theory[isigma,:],color=color_grid,lw=lw)

IMU=range(0,50,2)
for i,imu in enumerate(IMU):
    ax.plot(MU[imu]*np.ones(len(MU)),SIGMAS_S,Performance_theory[:,imu],color=color_grid,lw=lw)

#### example psychometric ###
ISIGMAS=[1,11,21,44]
for icolor,isigma in enumerate(ISIGMAS):
    ax.plot(MU,SIGMAS_S[isigma]*np.ones(len(MU)),Performance_theory[isigma,:],color=color_sigma[icolor])

#### example accuracy vs sigma ###
isr=21
imu=34
ax.plot(MU[imu]*np.ones(len(MU)),SIGMAS_S,Performance_theory[:,imu],color="black")


ax.set_ylabel("Stimulus fluctuations "+ r"$\sigma_S$",fontsize=fontsize)
ax.set_xlabel("Mean stimulus \n"+" evidence "+ r"$\mu$",fontsize=fontsize)
ax.set_zlabel("Probability of a \n rightward choice",fontsize=fontsize)
hp.yticks(ax,[0,0.5,1],fontsize=fontsize)
hp.xticks(ax,[-0.4,0,0.4],fontsize=fontsize)
ax.set_zticks([0,0.5,1])
ax.set_zticklabels(["0","0.5","1"],fontsize=fontsize)

def get_fixed_mins_maxs(mins, maxs):
    deltas = (maxs - mins) / 12.
    mins = mins + deltas / 4.
    maxs = maxs - deltas / 4.

    return [mins, maxs]


x_lim=get_fixed_mins_maxs(-0.4,0.4)
y_lim=get_fixed_mins_maxs(1,0)
z_lim=ax.set_zlim(get_fixed_mins_maxs(0,1))

ax.set_ylim(y_lim )
ax.set_xlim(x_lim)
ax.set_zlim(z_lim)


elev=21
azim=-145
ax.view_init(elev, azim)

axes=[ [],[],[],[],]
axes[0].append(ax)

###############Acuracy ##################


f=open(path+"pk_accuracy_sigmas.pickle","rb")
data=pickle.load(f)
f.close()
SIGMAS_S=data["SIGMAS_S"]
PK=data["PK"]
PRI=data["PRI"]
Performance=data["Performance"]
Performance_theory=data["Performance_theory"]
Performance_t0=data["Performance_t0"]
Performance_ts=data["Performance_ts"]
Performance_theory_T=data["Performance_theory_T"]
Performance_theory_sigma_i=data["Performance_theory_sigma_i"]
TS=data["TS"]
SIGMAS_I=data["SIGMAS_I"]

ax2 = fig.add_subplot(423)
axes[1].append(ax2)

axes[1][0].plot(SIGMAS_S,Performance,'ko',markersize=2)
axes[1][0].set_ylabel('Accuracy', fontsize=fontsize)
axes[1][0].plot(SIGMAS_S,Performance_t0,'-',color="lightgrey",lw=1)
axes[1][0].plot(SIGMAS_S,Performance_theory,'k-')

hp.remove_axis_bottom(axes[1][0])
axes[1][0].axhline(0.5,-1,SIGMAS_S[-1],color='grey',ls='--')
hp.yticks(axes[1][0],[0.5,0.75,1],['0.5','0.75','1'],fontsize=fontsize)
axes[1][0].set_xlim([0,1])

############################## P. revearsal ##################

ax3 = fig.add_subplot(425)
axes[2].append(ax3)
axes[2][0].axhline(0.5,-1,SIGMAS_S[-1],color='grey',ls='--')
axes[2][0].plot(SIGMAS_S,data["prl"]-(1-data["prr"]),color='lightgrey',lw=1)
axes[2][0].plot(SIGMAS_S,data["prl"],color='green')
axes[2][0].plot(SIGMAS_S,1-data["prr"],color='black')

axes[2][0].set_ylabel('Prob. Transition',fontsize=fontsize)
hp.yticks(axes[2][0],[0.0,0.5],['0','0.5'],fontsize=fontsize)
hp.remove_axis_bottom(axes[2][0])
axes[2][0].set_xlim([0,1])



# ###############PRI ##################

ax4 = fig.add_subplot(427)
axes[3].append(ax4)

hp.xticks(axes[3][0],[0,0.5,1],fontsize=fontsize)
axes[3][0].set_xlabel(r'Stimulus fluctuations $\sigma_s$',fontsize=fontsize)

axes[3][0].plot(SIGMAS_S,-np.array(PRI),'-',color='k')
axes[3][0].plot( [SIGMAS_S[0],SIGMAS_S[-1]],np.zeros(2),'--',color='grey')
axes[3][0].set_ylabel('Normalized PK slope', fontsize=fontsize)
hp.yticks(axes[3][0],[-1,0,1],fontsize=fontsize)
axes[3][0].set_xlim([0,1])
hp.remove_axis(axes[3][0])


# ############# PLOTTING KERNELS ################ 
isigmas_kernel=[11,21,-6]
icolors=[1,2,3]
w_pot=0.08
h_pot=0.15

ax_kernel1=fig.add_axes([0.2,0.6,w_pot,h_pot])
hp.kernel_plot(ax_kernel1,PK[isigmas_kernel[0]],1,color=color_sigma[1],linewidth=1,smoothing_window=10)
ax_kernel1.axis('off')
ylim=0.8
ax_kernel1.set_ylim(0.45,ylim)

ax_kernel1=fig.add_axes([0.28,0.57,w_pot,h_pot])
hp.kernel_plot(ax_kernel1,PK[isigmas_kernel[1]],1,color=color_sigma[2],linewidth=1,ylim=0.6,smoothing_window=10)
ax_kernel1.axis('off')
ax_kernel1.set_ylim(0.45,ylim)

ax_kernel1=fig.add_axes([0.4,0.53,w_pot,h_pot])
hp.kernel_plot(ax_kernel1,PK[isigmas_kernel[2]],1,color=color_sigma[3],linewidth=1,smoothing_window=10)
ax_kernel1.axis('off')
ax_kernel1.set_ylim(0.45,ylim)

for i,isigma in enumerate(isigmas_kernel): 
    axes[1][0].plot(SIGMAS_S[isigma],Performance[isigma],'o',markersize=3,color=color_sigma[i+1])



################### PC vs sigma different times #############

ax5 = fig.add_subplot(424)
i=1
axes[i].append(ax5)


dash=['--','-','-','--']
labels=["$P_{0.5s}$","$P_{2s}$","$P_{8s}$"]
index_TS=[4,10,15]
axes[i][1].plot(SIGMAS_S,Performance_t0,'-',color=color_T[0],label=r'$P_{0}$')
for it,t in enumerate(index_TS):
    axes[i][1].plot(SIGMAS_S,Performance_theory_T[t],'-',color=color_T[it+1],label=labels[it])
    
axes[i][1].plot(SIGMAS_S,Performance_ts,'-',color=color_T[-1],label=r'$P_{\infty}$')
hp.remove_axis_bottom(axes[0][0])
axes[i][1].set_xlabel('',fontsize=8)
axes[i][1].set_ylabel('Accuracy',fontsize=fontsize)
axes[i][1].legend(loc=[0.65,0.2],framealpha=0,fontsize=fontsize)
hp.yticks(axes[i][1],[0.5,0.75,1],['0.5','0.75','1'],fontsize=fontsize)
hp.remove_axis_bottom(axes[i][1])
axes[1][1].set_xlim([0,1])

# ################### PC vs sigma different sigma0 #############

ax5 = fig.add_subplot(426)
axes[2].append(ax5)
labels=[r'$\sigma_I=0.1$',r'$\sigma_I=0.3$',r'$\sigma_I=0.5$']
for isigma0,sigma0 in enumerate(SIGMAS_I):
    axes[2][1].plot(SIGMAS_S,Performance_theory_sigma_i[isigma0],'-',color=color_sigma_i[isigma0],label=labels[isigma0])
    
hp.remove_axis(axes[2][0])
axes[2][1].set_ylabel('Accuracy',fontsize=fontsize)
hp.yticks(axes[2][1],[0.5,0.75,1],['0.5','0.75','1'],fontsize=fontsize)
hp.remove_axis_bottom(axes[2][1])
axes[2][1].legend(loc=[0.6,0.25],framealpha=0,fontsize=fontsize)
axes[2][1].set_xlim([0,1])


############### Performace other models ################

ax6 = fig.add_subplot(428)
axes[3].append(ax6)

f=open(path+'PK_SIGMAS_performance_DDM.pickle','rb')
data=pickle.load(f)
f.close()

Performance_DDMA=data['Performance_DDMA']
Performance_DDMR=data['Performance_DDMR']
Performance_PI=data['Performance_PI']
SIGMAS_S=data['SIGMAS_S']
models=['PI_theoretical','DDMA','DDMR']
colors=[color_PI,color_DDMA,color_DDMR]
dash=['-','-','--']

axes[3][1].plot(SIGMAS_S,Performance_PI,dash[0],color=colors[0])
axes[3][1].plot(SIGMAS_S,Performance_DDMA,dash[1],color=colors[1])
axes[3][1].plot(SIGMAS_S,Performance_DDMR,dash[2],color=colors[2])
hp.remove_axis(axes[1][1])
axes[3][1].set_xlabel('$\sigma_s$',fontsize=fontsize)

hp.yticks(axes[3][1],[0.5,0.75,1],['0.5','0.75','1'],fontsize=fontsize)
hp.xticks(axes[3][1],[0.0,0.5,1],['0','0.5','1'],fontsize=fontsize)

axes[3][1].set_xlabel('$\sigma_s$',fontsize=fontsize)
axes[3][1].set_xlim([0,1])


axes[3][1].set_ylabel('Accuracy',fontsize=fontsize)
axes[3][1].set_xlabel(r'Stimulus fluctuations $\sigma_s$',fontsize=fontsize)
hp.remove_axis(axes[3][1])


################## POSITIONS ################ 
h1=0.3

axes[0][0].set_position([0.25,1-h1,0.6,h1])

w0=0.12
wf=0.05
ws=0.2
w=(1-w0-wf-ws)/2.

hs=0.05
hs2=0.1
h0=0.07

h=(1-h1-h0-2*hs-hs2)/3
iax2=0
for iax in range(1,len(axes)):
    axes[iax][iax2].set_position([w0,1-h1-iax*(hs+h)-hs2+hs,w,h])
iax2=1
for iax in range(1,len(axes)):
    axes[iax][iax2].set_position([w0+ws+w,1-h1-iax*(hs+h)-hs2+hs,w,h])




plt.show()
fig.savefig(path+'figure3_3d_pyhton.svg')

