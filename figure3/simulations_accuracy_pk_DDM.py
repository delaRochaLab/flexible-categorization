"""
@author: genis
This script computes the accuracy of the canonical models
for different values of stimulus fluctuations magnitude.
"""

import sys
sys.path.insert(0, '/home/genis/flexible-categorization/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt

PK=[]
NPKA=[]
PRI=[]

Nstim=int(1e5)
Nsigmas=50
T=2000
tau=200.
dt=tau/40.

NT=int(T/dt)
time=np.linspace(0,T,NT)

sigma_i=0.0
SIGMAS_S=np.linspace(0.05,1.0,Nsigmas)



###### Performance other models #####


Performance_DDMA=np.zeros(Nsigmas)
Performance_DDMA_err=np.zeros(Nsigmas)

Performance_DDMR=np.zeros(Nsigmas)
Performance_DDMR_err=np.zeros(Nsigmas)

Performance_PI=np.zeros(Nsigmas)
Performance_PI_err=np.zeros(Nsigmas)

mu=0.05
sigma_i=0.
tau=200
T=2000
bound=0.5

for isigma in range(len(SIGMAS_S)):
    print("Computing accuracy",isigma)
    stim_noise=SIGMAS_S[isigma]*np.random.randn(Nstim,NT)
    internal_noise=sigma_i*np.random.randn(Nstim,NT) #internal nopise
    d,xfinal=sd.simulation_DDMA(mu,stim_noise,internal_noise,bound,T,tau,0.)
    d=np.array(d)
    Performance_DDMA[isigma],Performance_DDMA_err[isigma]=ba.mean_and_error( (d+1)/2 )


for isigma in range(len(SIGMAS_S)):
    print("Computing accuracy",isigma)
    stim_noise=SIGMAS_S[isigma]*np.random.randn(Nstim,NT)
    internal_noise=sigma_i*np.random.randn(Nstim,NT) #internal nopise
    d,xfinal=sd.simulation_DDMR(mu,stim_noise,internal_noise,bound,T,tau,0.)
    d=np.array(d)
    Performance_DDMR[isigma],Performance_DDMR_err[isigma]=ba.mean_and_error( (d+1)/2 )


for isigma in range(len(SIGMAS_S)):
    print("Computing accuracy",isigma)
    stim_noise=SIGMAS_S[isigma]*np.random.randn(Nstim,NT)
    internal_noise=sigma_i*np.random.randn(Nstim,NT) #internal nopise
    d,xfinal=sd.simulation_PI(mu,stim_noise,internal_noise,T,tau,0.)
    d=np.array(d)
    Performance_PI[isigma],Performance_PI_err[isigma]=ba.mean_and_error( (d+1)/2 )

plt.figure()
plt.plot(SIGMAS_S,Performance_PI)
plt.plot(SIGMAS_S,Performance_DDMA)
plt.plot(SIGMAS_S,Performance_DDMR)
plt.show()




path="/home/genis/flexible-categorization/figure3/"
data={ "SIGMAS_S":SIGMAS_S,"T":T,
"sigma_i":sigma_i,"tau":tau,"mu":mu,
"Performance_DDMA":Performance_DDMA,"Performance_DDMA_err":Performance_DDMA_err,
"Performance_DDMR":Performance_DDMR,"Performance_DDMR_err":Performance_DDMR_err,
"Performance_PI":Performance_PI,"Performance_PI_err":Performance_PI_err,

}


f=open(path+"PK_SIGMAS_performance_DDM.pickle","wb")
pickle.dump(data,f)
f.close()
