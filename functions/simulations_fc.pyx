"""
@author: genis
"""

from cpython cimport array
import array
import numpy as np
import time as t
cimport numpy as np
from cython import boundscheck, wraparound
from libc.stdlib cimport malloc, free


@boundscheck(False)
@wraparound(False) 
def simulation_PI(double mu, double[:,:]  stim, double[:,:]  internal_noise, long T,double tau,double x0):
    '''
    Simulations of the Drift diffusion Model with reflecting bound
    coef_stim=[mu]
    stim: stimulus noise with dimension Ntrials, NT
    internal_noise:
    tau:time constant
    T stimulus duration
    double x0: initial condition
    
    '''

    
    cdef int Nstim=stim.shape[0]
    cdef int itrial,ipulse,it,it_i,auxT,auxTinter,NT,NTinter
    cdef double dt_sqrt,deltat,dt,x
    NT=internal_noise.shape[1]
    deltat=1.0*T/NT
    
    dt=deltat/tau
    dt_sqrt=np.sqrt(dt)

    aux=np.zeros(Nstim,dtype='d')
    cdef double [:] d=aux.copy()
    cdef double [:] xfinal=aux.copy()


    for itrial in range(Nstim):
        it=0
        x=x0
        for it in range(NT):
            x =x-(-mu)*dt+ dt_sqrt*(internal_noise[itrial][it]+stim[itrial][it])

        xfinal[itrial]=x
        d[itrial]=np.sign(x)
    return d,xfinal



@boundscheck(False)
@wraparound(False) 
def simulation_DDMA(double mu, double[:,:]  stim, double[:,:]  internal_noise, double bound, long T,double tau,double x0):
    '''
    Simulations of the Drift diffusion Model with absorbing bound
    coef_stim=[mu]
    stim: stimulus noise with dimension Ntrials, NT
    internal_noise:
    tau:time constant
    T stimulus duration
    double x0: initial condition
    
    '''

    
    cdef int Nstim=stim.shape[0]
    cdef int itrial,ipulse,it,it_i,auxT,auxTinter,NT,NTinter
    cdef double dt_sqrt,deltat,dt,x
    NT=internal_noise.shape[1]
    deltat=1.0*T/NT
    
    dt=deltat/tau
    dt_sqrt=np.sqrt(dt)

    aux=np.zeros(Nstim,dtype='d')
    cdef double [:] d=aux.copy()
    cdef double [:] xfinal=aux.copy()


    for itrial in range(Nstim):
        it=0
        x=x0
        while(it < NT and x<bound and x>-bound):
            x =x-(-mu)*dt+ dt_sqrt*(internal_noise[itrial][it]+stim[itrial][it])
            it=it+1

        xfinal[itrial]=x
        d[itrial]=np.sign(x)
    return d,xfinal


@boundscheck(False)
@wraparound(False) 
def simulation_DDMR(double mu, double[:,:]  stim, double[:,:]  internal_noise, double bound, long T,double tau,double x0):
    '''
    Simulations of the Drift diffusion Model with reflecting bound
    coef_stim=[mu]
    stim: stimulus noise with dimension Ntrials, NT
    internal_noise:
    tau:time constant
    T stimulus duration
    double x0: initial condition
    
    '''

    
    cdef int Nstim=stim.shape[0]
    cdef int itrial,ipulse,it,it_i,auxT,auxTinter,NT,NTinter
    cdef double dt_sqrt,deltat,dt,x
    NT=internal_noise.shape[1]
    deltat=1.0*T/NT
    
    dt=deltat/tau
    dt_sqrt=np.sqrt(dt)

    aux=np.zeros(Nstim,dtype='d')
    cdef double [:] d=aux.copy()
    cdef double [:] xfinal=aux.copy()


    for itrial in range(Nstim):
        it=0
        x=x0
        for it in range(NT):
            x =x-(-mu)*dt+ dt_sqrt*(internal_noise[itrial][it]+stim[itrial][it])
            if x>bound:
                x=bound
            elif x<-bound:
                x=-bound

        xfinal[itrial]=x
        d[itrial]=np.sign(x)
    return d,xfinal



@boundscheck(False)
@wraparound(False) 
def simulation_DDMA_traces(double mu, double[:,:]  stim, double[:,:]  internal_noise, double bound, long T,double tau,double x0):
    '''
    Simulations of the Drift diffusion Model with absorbing bound
    coef_stim=[mu]
    stim: stimulus noise with dimension Ntrials, NT
    internal_noise:
    tau:time constant
    T stimulus duration
    double x0: initial condition
    
    '''

    
    cdef int Nstim=stim.shape[0]
    cdef int itrial,ipulse,it,it_i,auxT,auxTinter,NT,NTinter
    cdef double dt_sqrt,deltat,dt,x
    NT=internal_noise.shape[1]
    deltat=1.0*T/NT
    
    dt=deltat/tau
    dt_sqrt=np.sqrt(dt)

    aux=np.zeros(Nstim,dtype='d')
    cdef double [:] d=aux.copy()
    aux2=np.zeros((Nstim,internal_noise.shape[1]+1),dtype='d')
    cdef double [:,:] xfinal=aux2.copy()

    for itrial in range(Nstim):
        it=0
        xfinal[itrial][0]=x0
        while(it < NT and xfinal[itrial][it] >-bound and xfinal[itrial][it] <bound):
            xfinal[itrial][it+1] =xfinal[itrial][it]-(-mu)*dt+ dt_sqrt*(internal_noise[itrial][it]+stim[itrial][it])
            it=it+1
        while(it < NT):
            xfinal[itrial][it+1] =xfinal[itrial][it]
            it=it+1

        d[itrial]=np.sign(xfinal[itrial][it+1])
    return d,xfinal



@boundscheck(False)
@wraparound(False) 
def simulation_DDMR_traces(double mu, double[:,:]  stim, double[:,:]  internal_noise, double bound, long T,double tau,double x0):
    '''
    Simulations of the Drift diffusion Model with absorbing bound
    coef_stim=[mu]
    stim: stimulus noise with dimension Ntrials, NT
    internal_noise:
    tau:time constant
    T stimulus duration
    double x0: initial condition
    
    '''

    
    cdef int Nstim=stim.shape[0]
    cdef int itrial,ipulse,it,it_i,auxT,auxTinter,NT,NTinter
    cdef double dt_sqrt,deltat,dt,x
    NT=internal_noise.shape[1]
    deltat=1.0*T/NT
    
    dt=deltat/tau
    dt_sqrt=np.sqrt(dt)

    aux=np.zeros(Nstim,dtype='d')
    cdef double [:] d=aux.copy()
    aux2=np.zeros((Nstim,internal_noise.shape[1]+1),dtype='d')
    cdef double [:,:] xfinal=aux2.copy()

    for itrial in range(Nstim):
        it=0
        xfinal[itrial][0]=x0
        while(it < NT ):
            xfinal[itrial][it+1] =xfinal[itrial][it]-(-mu)*dt+ dt_sqrt*(internal_noise[itrial][it]+stim[itrial][it])
            it=it+1
            if xfinal[itrial][it] <-bound:
                xfinal[itrial][it]=-bound

            if xfinal[itrial][it]>bound:
                xfinal[itrial][it]=bound
                       


        d[itrial]=np.sign(xfinal[itrial][it+1])
    return d,xfinal




@boundscheck(False)
@wraparound(False) 
def simulation_PI_traces(double mu, double[:,:]  stim, double[:,:]  internal_noise, long T,double tau,double x0):
    '''
    Simulations of the Drift diffusion Model with absorbing bound
    coef_stim=[mu]
    stim: stimulus noise with dimension Ntrials, NT
    internal_noise:
    tau:time constant
    T stimulus duration
    double x0: initial condition
    
    '''

    cdef int Nstim=stim.shape[0]
    cdef int itrial,ipulse,it,it_i,auxT,auxTinter,NT,NTinter
    cdef double dt_sqrt,deltat,dt,x
    NT=internal_noise.shape[1]
    deltat=1.0*T/NT
    
    dt=deltat/tau
    dt_sqrt=np.sqrt(dt)

    aux=np.zeros(Nstim,dtype='d')
    cdef double [:] d=aux.copy()
    aux2=np.zeros((Nstim,internal_noise.shape[1]+1),dtype='d')
    cdef double [:,:] xfinal=aux2.copy()

    for itrial in range(Nstim):
        it=0
        xfinal[itrial][0]=x0
        while(it < NT ):
            xfinal[itrial][it+1] =xfinal[itrial][it]-(-mu)*dt+ dt_sqrt*(internal_noise[itrial][it]+stim[itrial][it])
            it=it+1
                       


        d[itrial]=np.sign(xfinal[itrial][it])
    return d,xfinal




@boundscheck(False)
@wraparound(False) 
def simulation_DW(double[:] coef_stim, double[:,:]  stim, double[:,:]  internal_noise, long T,double tau,double x0):
    '''
    Simulations of the Double Well Model wiht coeficient coef_stim
    coef_stim=[mu,c2,c4]
    stim: stimulus noise with dimension Ntrials, NT
    internal_noise:
    tau:time constant
    T stimulus duration
    double x0: initial condition
    
    '''
    
    cdef int Nstim=stim.shape[0]
    cdef int itrial,ipulse,it,it_i,auxT,auxTinter,NT,NTinter
    cdef double dt_sqrt,deltat,dt,x
    NT=internal_noise.shape[1]
    deltat=1.0*T/NT
    
    dt=deltat/tau
    dt_sqrt=np.sqrt(dt)

    aux=np.zeros(Nstim,dtype='d')
    cdef double [:] d=aux.copy()
    cdef double [:] xfinal=aux.copy()


    for itrial in range(Nstim):
        it_i=0
        x=x0
        for it in range(NT):
            x =x-(-coef_stim[0]-coef_stim[1]*x+coef_stim[2]*(x**3))*dt+ dt_sqrt*(internal_noise[itrial][it]+stim[itrial][it])
            # print 'it',it_i,x        
        xfinal[itrial]=x
        d[itrial]=np.sign(x)
        # print x
    return d,xfinal



@boundscheck(False)
@wraparound(False) 
def simulation_DW_traces(double[:] coef_stim, double[:,:]  stim, double[:,:]  internal_noise, long T,double tau,double x0):
    '''
    Simulations of the Double Well Model wiht coeficient coef_stim
    coef_stim=[mu,c2,c4]
    stim: stimulus noise with dimension Ntrials, NT
    internal_noise:
    tau:time constant
    T stimulus duration
    double x0: initial condition
    
    '''
    
    cdef int Nstim=stim.shape[0]
    cdef int itrial,ipulse,it,it_i,auxT,auxTinter,NT,NTinter
    cdef double dt_sqrt,deltat,dt,x
    NT=internal_noise.shape[1]
    deltat=1.0*T/NT
    
    dt=deltat/tau
    dt_sqrt=np.sqrt(dt)

    aux=np.zeros(Nstim,dtype='d')
    cdef double [:] d=aux.copy()
    aux2=np.zeros((Nstim,internal_noise.shape[1]+1),dtype='d')
    cdef double [:,:] xfinal=aux2.copy()

    for itrial in range(Nstim):
        it_i=0
        xfinal[itrial][0]=x0
        for it in range(NT):
            xfinal[itrial][it+1] =xfinal[itrial][it]-(-coef_stim[0]-coef_stim[1]*xfinal[itrial][it]+coef_stim[2]*(xfinal[itrial][it]**3))*dt+ dt_sqrt*(internal_noise[itrial][it]+stim[itrial][it])
        d[itrial]=np.sign(xfinal[itrial][-1])
    return d,xfinal



@boundscheck(False)
@wraparound(False) 
def simulation_frames_tau(double[:] coef_stim , double[:,:] stim,double[:,:]  internal_noise, double T,double tau,double x0):
    '''
    Simulates (euler method) a diffusion process in a DW potential that changes in frames of duration T. 
    
    coef_stim is the array of potential coef=[mu,c2,c4] #here mu is taken from stim 

     for each trial and frame. it has dimension (Ntrials,Nframes,3). 
    3 is the number of coeficiants for the DW mu,c2,c4
    
    internal_noise is the diffusion part of the equation.
    
    tau is time constatn.
    
    x0 initial position.
    '''
    
    cdef int Npulses=stim.shape[1]
    cdef int Nstim=stim.shape[0]
    cdef int itrial,ipulse,it,it_i,auxT,auxTinter,NT,NTinter
    cdef double dt_sqrt,deltat,dt,x
    NT=internal_noise.shape[1]/Npulses
    deltat=1.0*T/NT
    
   
    dt=deltat/tau
    dt_sqrt=np.sqrt(dt)
    aux=np.zeros(Nstim,dtype='d')
    cdef double [:] d=aux.copy()
    cdef double [:] xfinal=aux.copy()

    for itrial in range(Nstim):
        it_i=0
        x=x0
        for ipulse in range(Npulses):
            for it in range(NT):
                x =x-(-stim[itrial][ipulse]-coef_stim[1]*x+coef_stim[2]*(x**3))*dt+ dt_sqrt*internal_noise[itrial][it_i]
                it_i=it_i+1
        xfinal[itrial]=x
        if x<0:
            d[itrial]=-1
        else:
            d[itrial]=1
    return d,xfinal



@boundscheck(False)
@wraparound(False) 
def simulation_frames_tau_traces(double[:] coef_stim , double[:,:] stim,double[:,:]  internal_noise, double T,double tau,double x0):
    '''
    Same as simulation_frames_tau but it returns the 
    singles trial traces
    
    '''
    
    cdef int Npulses=stim.shape[1]
    cdef int Nstim=stim.shape[0]
    cdef int itrial,ipulse,it,it_i,NT,NTinter
    cdef double deltat_sqrt
    

    NT=internal_noise.shape[1]/Npulses
    deltat=1.0*T/NT

    # NTinter=int(round(1.0*Tinter/deltat))
    # dt=deltat/tau
    
    dt=deltat/tau
    dt_sqrt=np.sqrt(dt)




    aux=np.zeros(Nstim,dtype='d')

    cdef double [:] d=aux.copy()
    aux2=np.zeros((Nstim,internal_noise.shape[1]+1),dtype='d')
    cdef double [:,:] xfinal=aux2.copy()


    for itrial in range(Nstim):
        it_i=0
        x=x0
        ipulse=0
        it_i=1


        for ipulse in range(Npulses):
            for it in range(NT):
                xfinal[itrial][it_i] =xfinal[itrial][it_i-1]-(-stim[itrial][ipulse]-coef_stim[1]*xfinal[itrial][it_i-1]+coef_stim[2]*(xfinal[itrial][it_i-1]**3))*dt+ dt_sqrt*internal_noise[itrial][it_i-1]
                it_i=it_i+1
       
        if xfinal[itrial][it_i-1]<0:
            d[itrial]=-1
        else:
            d[itrial]=1

    return d,xfinal



# @boundscheck(False)
# @wraparound(False) 
def simulation_frames_tau_delay_traces(double[:] coef_stim , double[:] coef_No_stim, double[:,:] stim, double[:,:]  internal_noise, double[:,:,:]  stim_noise, double T,double Tdelay,double tau,double x0):
    '''
    Simulates (euler method) a diffusion process in a DW potential that changes in frames of duration T. 
    
    coef_stim is the array of potential coef=[mu,c2,c4] #here mu is taken from stim 
    coef_No_stim is the array of potential coeficients during the delay between pulses

    
    internal_noise and stim noise  are the diffusion part of the equation.
    
    tau is time constatn.internal_noise.shape[1]
    
    x0 initial position.
    '''
    
    cdef int Npulses=stim.shape[1]
    cdef int Nstim=stim.shape[0]
    cdef int itrial,ipulse,it,it_i,auxT,auxTinter,NT,NTinter
    cdef double dt_sqrt,deltat,dt,x
    NT=stim_noise.shape[2]
    NTdelay=internal_noise.shape[1]-2*NT
    deltat=1.0*T/NT 
    dt=deltat/tau
    dt_sqrt=np.sqrt(dt)

    aux=np.zeros(Nstim,dtype='d')
    cdef double [:] d=aux.copy()
    aux2=np.zeros((Nstim,internal_noise.shape[1]+1),dtype='d')
    cdef double [:,:] xfinal=aux2.copy()
    
    for itrial in range(Nstim):
        it_i=1
        xfinal[itrial][0]=x0
        for ipulse in range(Npulses):
            for it in range(NT):
                xfinal[itrial][it_i] =xfinal[itrial][it_i -1]-(-stim[itrial][ipulse]-coef_stim[1]*xfinal[itrial][it_i -1]+coef_stim[2]*(xfinal[itrial][it_i -1]**3))*dt+ dt_sqrt*(internal_noise[itrial][it_i -1]+stim_noise[itrial][ipulse][it])
                it_i=it_i+1
            if ipulse<Npulses-1:
                for it in range(NTdelay):
                    xfinal[itrial][it_i] =xfinal[itrial][it_i -1]-(-stim[itrial][ipulse]-coef_stim[1]*xfinal[itrial][it_i -1]+coef_stim[2]*(xfinal[itrial][it_i -1]**3))*dt+ dt_sqrt*(internal_noise[itrial][it_i -1])
                    it_i=it_i+1
                   
        if xfinal[itrial][it_i-1]<0:
            d[itrial]=-1
        else:
            d[itrial]=1
    return d,xfinal




@boundscheck(False)
@wraparound(False) 
def simulation_DDMCB_traces(double mu, double[:,:]  stim, double[:,:]  internal_noise,double[:] bound,long T,double tau,double x0):
    '''
    Simulations of the Drift diffusion Model with collapsing bound
    coef_stim=[mu]
    stim: stimulus noise with dimension Ntrials, NT
    internal_noise:
    tau:time constant
    T stimulus duration
    double x0: initial condition
    bound: Vector of size NT with the bound value for each time
    
    '''

    
    cdef int Nstim=stim.shape[0]
    cdef int itrial,ipulse,it,it_i,auxT,auxTinter,NT,NTinter
    cdef double dt_sqrt,deltat,dt,x
    NT=internal_noise.shape[1]
    deltat=1.0*T/NT
    
    dt=deltat/tau
    dt_sqrt=np.sqrt(dt)

    aux=np.zeros(Nstim,dtype='d')
    cdef double [:] d=aux.copy()
    aux2=np.zeros((Nstim,internal_noise.shape[1]+1),dtype='d')
    cdef double [:,:] xfinal=aux2.copy()

    for itrial in range(Nstim):
        it=0
        xfinal[itrial][0]=x0
        
        while(it < NT and xfinal[itrial][it] >-bound[it] and xfinal[itrial][it] <bound[it]):
            xfinal[itrial][it+1] =xfinal[itrial][it]-(-mu)*dt+ dt_sqrt*(internal_noise[itrial][it]+stim[itrial][it])
            it=it+1
        while(it < NT):
            xfinal[itrial][it+1] =xfinal[itrial][it]
            it=it+1

        d[itrial]=np.sign(xfinal[itrial][it+1])
    return d,xfinal



@boundscheck(False)
@wraparound(False) 
def simulation_DDMCB(double mu, double[:,:]  stim, double[:,:]  internal_noise,double[:] bound,long T,double tau,double x0):
    
    '''
    Simulations of the Drift diffusion Model with collapsing bound
    coef_stim=[mu]
    stim: stimulus noise with dimension Ntrials, NT
    internal_noise:
    tau:time constant
    T stimulus duration
    double x0: initial condition
    
    '''

    
    cdef int Nstim=stim.shape[0]
    cdef int itrial,ipulse,it,it_i,auxT,auxTinter,NT,NTinter
    cdef double dt_sqrt,deltat,dt,x
    NT=internal_noise.shape[1]
    deltat=1.0*T/NT
    
    dt=deltat/tau
    dt_sqrt=np.sqrt(dt)

    aux=np.zeros(Nstim,dtype='d')
    cdef double [:] d=aux.copy()
    cdef double [:] xfinal=aux.copy()


    for itrial in range(Nstim):
        it=0
        x=x0
        while(it < NT and x >-bound[it] and x <bound[it]):
            x =x-(-mu)*dt+ dt_sqrt*(internal_noise[itrial][it]+stim[itrial][it])
            it=it+1

        xfinal[itrial]=x
        d[itrial]=np.sign(x)
    return d,xfinal