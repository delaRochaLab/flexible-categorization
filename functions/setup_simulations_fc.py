#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon May 29 17:49:02 2017

@author: genis
"""

from distutils.core import setup
from Cython.Build import cythonize
import matplotlib.pyplot as plt
import numpy

setup(
    ext_modules=cythonize("simulations_fc.pyx"),
    include_dirs=[numpy.get_include()],
)