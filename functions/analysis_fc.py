'''

@author: genis
'''

import numpy as np
from scipy.optimize import curve_fit
import numpy as np
import math
from sklearn.metrics import roc_curve, auc
from sklearn.linear_model import LogisticRegression

from scipy.special import erf
import statsmodels.discrete.discrete_model as sm
from scipy.stats import norm



def kernel_err(stim,d,error=False,Nboot=500):
    '''
    Computes the kernel and the standard error with bootstrap.
    inputs:
    stim: 2D-Array of stimulus
    d: 1-D array of decisions (-1,1)
    
    outputs:
    kernel: Dictionary with kernel and error_kernel
    '''
    Nframe=len(stim[0])
    Nstim=len(stim)
    kernel={'kernel':np.zeros(Nframe),'error':np.zeros(Nframe)}
    area_boot=np.zeros(Nboot)
    PRI=np.zeros(Nboot)
    PRI2=np.zeros(Nboot)
    area_boot2=np.zeros(Nboot) ## it converts the negative are in positive.
    a_p=np.zeros(Nboot)
    a_r=np.zeros(Nboot)
    if not error: 
        aux_kernel=np.zeros(Nframe)    
        for iframe in range(Nframe):
            fpr,tpr,_=roc_curve(d,stim[:,iframe])
            aux_kernel[iframe] = auc(fpr, tpr) 
        kernel['kernel']=aux_kernel
        return kernel
    else:
        
        aux_kernel=np.zeros((Nframe,Nboot))
        indexs=np.random.randint(0,Nstim,(Nboot,Nstim))

    
        for iboot in range(Nboot):
            if iboot%100==0:
                print(iboot)
            for iframe in range(Nframe):
                fpr,tpr,_=roc_curve(d[indexs[iboot]],stim[indexs[iboot],iframe])
                aux_kernel[iframe][iboot] = auc(fpr, tpr)
            
            
        for iframe in range(Nframe):
            kernel['kernel'][iframe]=np.mean(aux_kernel[iframe])
            kernel['error'][iframe]=np.std(aux_kernel[iframe])
        
        return kernel


def PK_slope(kernel):
    '''
    Compute the slope of the PK:
    PRR=integral( kernel*f(t))
    with f(t)=1-a*t with a such as f(T)=-1 T stimulus duration
    positive recency
    zero flat
    negative primacy
    '''
    aux=np.linspace(1,-1,len(kernel))
    kernel=kernel-0.5
    aux_kernel=(kernel)/(np.sum(kernel))
    return -np.sum(aux_kernel*aux)

def total_area_kernel_PInormalize(kernel):
    '''
    Compute the PK area normalized by the area of a PI 
    '''
    nframes=len(kernel)
    area_pi=nframes*( 0.5+2/np.pi*np.arctan(1/np.sqrt(2*nframes-1)) ) -0.5*nframes

    return np.sum(kernel-0.5)/area_pi


def mean_and_error(vector,z=1):
    '''
    mean and error according to binomial distribution
    '''
    m=np.mean(vector)
    return m,z*np.sqrt(m*(1-m)/len(vector))  



def make_control_stim(mu,sigma,T,N):
    '''
    it returns stimulus with exact mean mu 
    and std sigma
    '''
    if N==1:
        stim=np.random.randn(T)
        m=np.mean(stim)
        s=np.std(stim)
        stim=mu+((stim-m)/s)*sigma
    else:
        stim=np.random.randn(N,T)
        for itrial in range(N):
            m=np.mean(stim[itrial])
            s=np.std(stim[itrial])
            stim[itrial]=mu+((stim[itrial]-m)/s)*sigma
    return stim
 

def running_average(x,window,mode='valid'):
    y=np.convolve(x, np.ones((window))/window, mode=mode)
    return y


def PRI_with_frames(PC,COH,N,confidence_interval=False):
    '''
    Primacy rececncy index computed from PC for kiani 2013 and Maryam 2018 data
    PC array of Probabilities of Correct
    COH array with the values of the coherences: dimensions: Number of possible trials x 2 (number of pulses)
    N array with the nu
    if confidence_interval is true it return PRI with confidence intervals and the sm with the logistic
    regression results 
    
    '''
    data_mu1=[x[0] for ix,x in enumerate(COH) for i in range(N[ix])]
    data_mu2=[x[1] for ix,x in enumerate(COH) for i in range(N[ix])]

    d=np.array([])
    for icoh in range(len(PC)):
        Ncorrect=int(round(PC[icoh]*N[icoh]))
        d=np.concatenate(( d,np.ones(Ncorrect),np.zeros(N[icoh]-Ncorrect)))


    X=np.ones((len(data_mu1),3))
    X[:,1]=np.array(data_mu1)
    X[:,2]=np.array(data_mu2)

    logit=sm.Logit(d,X)
    fit2pulse=logit.fit()
    beta2pulse=fit2pulse.params
    beta2pulse_stderr=fit2pulse.bse
    pvalues_2pulse=fit2pulse.pvalues

    if confidence_interval:
        N_conf_interval=10000
        beta1=beta2pulse[1]+beta2pulse_stderr[1]*np.random.randn(N_conf_interval)
        beta2=beta2pulse[2]+beta2pulse_stderr[2]*np.random.randn(N_conf_interval)

        aux=(beta1-beta2)/(beta1+beta2)
        PRI=np.mean(aux)
        PRI_std_err=np.std(aux)
        conf_interval=[PRI-1.96*PRI_std_err,PRI+1.96*PRI_std_err]
        return PRI,conf_interval,fit2pulse

    else:
        return (beta2pulse[1]-beta2pulse[2])/(beta2pulse[1]+beta2pulse[2])

