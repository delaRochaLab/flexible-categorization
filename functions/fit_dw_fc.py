#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 28 17:50:18 2017

@author: genis
"""
import sys
sys.path.insert(0, '/home/genis/Flexible-Categoritzation/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import math
from sklearn.metrics import roc_curve, auc
from sklearn.linear_model import LogisticRegression
from math import exp,sqrt,cos,acos,erf,fabs
from scipy.optimize import minimize
import glob 
import multiprocessing as mpl


def potential_DW(x,coef):
    '''
    DW potential at x with coeficients coef
    coef[0]=mu
    coef[1]=c2
    coef[2]=c4
    '''
    return -coef[0]*x-0.5*coef[1]*x**2+0.25*coef[2]*x**4


def potential_DW_prima(x,coef):
    '''
    First derivative of the DW potential at x with coeficients coef
    coef[0]=mu
    coef[1]=c2
    coef[2]=c4
    '''
    return -coef[0]-coef[1]*x+coef[2]*x**3

def potential_DW_prima2(x,coef):
    '''
    First derivative of the DW potential at x with coeficients coef
    coef[0]=mu
    coef[1]=c2
    coef[2]=c4
    '''
    return -coef[1]+3.*coef[2]*x**2 


def Initial_transition( coef, xL, x0, xR, sigma):
    '''
    Computes the probability to visit the right attractor when
    the initial position is x0 and the attractor are in xL and xR
    for a levels of noise sigma.
    '''

    c2_aux=coef[1]
    c2_sqrt=sqrt(c2_aux)
    mu=coef[0] 
    return ( erf(c2_sqrt/sigma*(x0+mu/c2_aux)) - erf(c2_sqrt/sigma*(xL+mu/c2_aux)) )/(  erf(c2_sqrt/sigma*(xR+mu/c2_aux)) - erf(c2_sqrt/sigma*(xL+mu/c2_aux)) )
 

# def Initial_transition2( coef, roots, x0, sigma):
#     '''
#     Computes the probability to visit the right attractor when
#     the initial position is x0 and the attractor are in xL and xR
#     for a levels of noise sigma.
#     '''
#     if len(roots)==1:
#         return (np.sign(coef[0])+1)/2
#     elif len(roots)==3:
#         xL=roots[0]
#         xR=roots[2]
#         c2_aux=coef[1]
#         c2_sqrt=sqrt(c2_aux)
#         mu=coef[0] 
#         return ( erf(c2_sqrt/sigma*(x0+mu/c2_aux)) - erf(c2_sqrt/sigma*(xL+mu/c2_aux)) )/(  erf(c2_sqrt/sigma*(xR+mu/c2_aux)) - erf(c2_sqrt/sigma*(xL+mu/c2_aux)) )
#     else: 
#         return 2*np.ones(2)


def Transition_probabilites(coef,xL,xM,xR,sigma,t):
    '''
    Computes the transistion probability to reamin in R 
    after t and the probabilituy to find the system in L
    given that it starts in R after a time t.
    Important t is in units of tau t=Tframe/tau where Tframe is 
    the time in ms
    '''

    kxRxL,kxLxR=Transition_rates(coef,xL,xM,xR,sigma)
    try:
        prs=kxRxL/(kxLxR+kxRxL)
    except ZeroDivisionError: 
        #The limit of prs when sigma tends to 0 depends goes to 1 if mu>0
        if coef[0]>0:
            prs=1.
        else:
            prs=0.

    try:
        aux=exp(-(kxLxR+kxRxL)*t)
        prr=prs+aux*(1-prs)
        prl=prs*(1-aux)
    except OverflowError:
        print(-(kxLxR+kxRxL)*t,t,kxLxR,"error")
        prl=prs    

    return prr,prl

# def Transition_probabilities_logfunction(coef,sigma,t):
#     roots=roots_DW_fast(coef)
#     if len(roots)==1: #if there is only one stable state
#         if roots[0]>0:
#             return 1,1 
#         else:
#             return 0,0
#     else:
#         return Transition_probabilites(coef,roots[0],roots[1],roots[2],sigma,t)




def Transition_rates(coef,xL,xM,xR,sigma):
    '''
    Transistions rates from R to L (kxLxR) and from L to R (kxRxL).
    coef is an array with the potential coeficiant mu, c2 and c4
    xL and xR are the positions of the attractors
    xM is the position of maximum between the two attractors
    sigma is the level of noise 
    '''
    D=1.*sigma**2/2.0

    delta_xLxM=potential_DW(xM,coef)- potential_DW(xL,coef)        
    delta_xRxM=potential_DW(xM,coef)- potential_DW(xR,coef)

    aux_kxRxL=sqrt(fabs(potential_DW_prima2(xM,coef)*potential_DW_prima2(xL,coef)))/(2.0*np.pi)
    aux_kxLxR=sqrt(fabs(potential_DW_prima2(xM,coef)*potential_DW_prima2(xR,coef)))/(2.0*np.pi)
    kxLxR=aux_kxLxR*exp(-delta_xRxM/D)
    kxRxL=aux_kxRxL*exp(-delta_xLxM/D)

    return kxRxL,kxLxR


def roots_DW(coef):
    '''
    Positions of the L and R attractor as well as 
    the intermediate stable state 
    '''
    p=np.zeros(4)
    p[0]=coef[2]
    p[1]=0
    p[2]=-coef[1]
    p[3]=-coef[0]
    roots=np.roots(p)
    roots_real_index=np.where(np.imag(roots)==0)
    roots=np.real(roots[roots_real_index])
    return np.sort(roots)

def roots_DW_fast(coef):
    '''
    Analytical positions of the L and R attractor as well as 
    the intermediate stable state. 
    d=-mu
    c=-c2
    a=c4
    '''
    d=-coef[0]
    c=-coef[1]
    a=coef[2]
    delta=-4.*a*c**3-27*(a**2)*d**2
    if delta <0:
        return [-np.sign(d)]
    else:
        p=c/a
        q=d/a
        phi=acos( sqrt(  (27./4.)*(q**2)/(-p**3) ) )
        # k=np.array(range(3))
        roots=[ 2*sqrt(-p/3.)*cos( phi/3.+2.0943951*k) for k in range(3)]
        sign=np.sign(-q)
        if sign>0:
            return [sign*roots[1],sign*roots[2],sign*roots[0]] #id check the order is always like this we do not need to sort.
        elif sign==0:
            return [roots[1],roots[2],roots[0]]
        else:
            return [sign*roots[0],sign*roots[2],sign*roots[1]]

# def max_mu(coef):
#     '''
#     Analytical positions of the L and R attractor as well as 
#     the intermediate stable state. 
#         d=-mu
#     c=-c2
#     a=c4
#     '''
#     d=-coef[0]
#     c=-coef[1]
#     a=coef[2]
#     # delta=-4.*a*c**3-27*(a**2)*d**2
    
#     return np.sqrt(-4.*c**3/(27.*a))



# def PR_DW_frames_psycho(coef,stim,stim_max,stim_min,Tframe,sigma,tau=1,k=1.,x0=0.):
#     '''
#     Probability of Right for a DW with coeficients coef. 
#     Assuming that the stimulus linearly change the mu coeficitent of the
#     potential mu=k*stim. Stim is an array of dimension Ntrial,Nframes
#     x0 is the initial position of the decision variable.
#     IMPORTANT stim is an array
#     '''

#     Ntrial=len(stim)
#     Nframes=len(stim[0])
#     if stim is list:
#         mu=[k*s for s in stim]
#     else:
#         mu=k*stim

#     t=1.0*Tframe/tau
#     PR=np.zeros(Ntrial)
#     for itrial in range(Ntrial):
#         coef[0]=mu[itrial][0]
#         roots=roots_DW_fast(coef)
#         #first frame is special because we need to compute the intial transition    
#         if len(roots)==1: #if there is only one stable state
#             if roots[0]>0:
#                 pr=1
#             else:
#                 pr=0
            
#         elif len(roots) is not 3:
#             print coef[0],coef[1],coef[2]
#             print "complex numbers"
#             return -1,-1,-1,-1,-1

#         else:
#             pr=Initial_transition(coef,roots[0],x0,roots[2],sigma)
#             # print pr,x0
#             prr,prl=Transition_probabilites(coef,roots[0],roots[1],roots[2],sigma,t)
#             pr=pr*prr+(1-pr)*prl
#         #Computes the temporal evolution of the pr 
#         for iframe in range(1,len(mu[itrial])):
#             coef[0]=mu[itrial][iframe]
#             roots=roots_DW_fast(coef)
#             if len(roots)==1: #if there is only one stable state
#                 if roots[0]>0:
#                     prr=1
#                     prl=1
#                 else:
#                     prr=0
#                     prl=0
                
#             elif len(roots) is not 3:
#                 print roots[0],roots[1],roots[2],coef
#                 print "complex numbers"
#                 return -1,-1,-1,-1,-1

#             else:
#                 prr,prl=Transition_probabilites(coef,roots[0],roots[1],roots[2],sigma,t)
#             pr=pr*prr+(1-pr)*prl
#         PR[itrial]=pr

#     return PR




def Performance_sigma_tau(coef,T,sigmas_s,tau=1,x0=0.,sigma_i=0.):
    '''
    Probability of Right for a DW with coeficients coef=[mu,c2,c4] 
    x0 is the initial position of the decision variable.
    reut
    '''

    t=1.0*T/tau
    pr0=np.zeros(len(sigmas_s))
    pr=np.zeros(len(sigmas_s))
    prl=np.zeros(len(sigmas_s))
    plr=np.zeros(len(sigmas_s))
    prr=np.zeros(len(sigmas_s))
    prs=np.zeros(len(sigmas_s))
    for isigma in range(len(sigmas_s)):
        s=np.sqrt(sigmas_s[isigma]**2+sigma_i**2)
        roots=roots_DW_fast(coef)
        #first frame is special because we need to compute the intial transition    
        if len(roots)==1: #if there is only one stable state
            if roots[0]>0:
                pr0=1
            else:
                pr0=0
            
        elif len(roots) is not 3:
            print(coef[0],coef[1],coef[2])
            print("complex numbers")
            return -1,-1,-1,-1,-1

        else:
            pr0[isigma]=Initial_transition(coef,roots[0],x0,roots[2],s)
            prr[isigma],prl[isigma]=Transition_probabilites(coef,roots[0],roots[1],roots[2],s,t)
            pr[isigma]=pr0[isigma]*prr[isigma]+(1-pr0[isigma])*prl[isigma]
            kxRxL,kxLxR=Transition_rates(coef,roots[0],roots[1],roots[2],s)
            try:
                prs[isigma]=kxRxL/(kxRxL+kxLxR)
            except ZeroDivisionError:
                 prs[isigma]=np.NaN

    return pr,pr0,prr,prl,prs





def PR_DW_frames(coef,stim,Tframe,sigma,tau=1,k=1.,x0=0.):
    '''
    Probability of Right for a DW with coeficients coef. 
    Assuming that the stimulus linearly change the mu coeficitent of the
    potential mu=k*stim. Stim is an array of dimension Ntrial,Nframes
    x0 is the initial position of the decision variable.
    IMPORTANT stim is an array
    '''
    Ntrial=len(stim)
    Nframes=len(stim[0])
    if stim is list:
        mu=[k*s for s in stim]
    else:
        mu=k*stim
    t=1.0*Tframe/tau
    PR=np.zeros(Ntrial)
    for itrial in range(Ntrial):
        coef[0]=mu[itrial][0]
        roots=roots_DW_fast(coef)
        #first frame is special because we need to compute the intial transition    
        if len(roots)==1: #if there is only one stable state
            if roots[0]>0:
                pr=1
            else:
                pr=0
            
        elif len(roots) is not 3:
            print(coef[0],coef[1],coef[2])
            print("complex numbers")
            return -1,-1,-1,-1,-1

        else:
            pr=Initial_transition(coef,roots[0],x0,roots[2],sigma)
            prr,prl=Transition_probabilites(coef,roots[0],roots[1],roots[2],sigma,t)
            pr=pr*prr+(1-pr)*prl
        #Computes the temporal evolution of the pr 
        for iframe in range(1,len(mu[itrial])):
            coef[0]=mu[itrial][iframe]
            roots=roots_DW_fast(coef)
            if len(roots)==1: #if there is only one stable state
                if roots[0]>0:
                    prr=1
                    prl=1
                else:
                    prr=0
                    prl=0
                
            elif len(roots) is not 3:
                print(roots[0],roots[1],roots[2],coef)
                print("complex numbers")
                return -1,-1,-1,-1,-1

            else:
                prr,prl=Transition_probabilites(coef,roots[0],roots[1],roots[2],sigma,t)
            pr=pr*prr+(1-pr)*prl
        PR[itrial]=pr

    return PR


# def PR_DW_frames_parallel(ARGS):
#     '''
#     Probability of Right for a DW with coeficients coef. 
#     Assuming that the stimulus linearly change the mu coeficitent of the
#     potential mu=k*stim. Stim is an array of dimension Ntrial,Nframes
#     x0 is the initial position of the decision variable.
#     IMPORTANT stim is an array
#     ARGS: coef,stim,Tframe,sigma,tau,k,x0
#     '''
#     coef=ARGS[0]
#     stim=np.array([ARGS[1]])
#     Tframe=ARGS[2]
#     sigma=ARGS[3]
#     tau=ARGS[4]
#     k=ARGS[5]
#     x0=ARGS[6]

#     return PR_DW_frames(coef,stim,Tframe,sigma,tau,k,x0)





def PR_DW_diff_number_frames(coef,stim,Tframe,sigma,tau=1,k=1.,x0=0.):
    '''
    Probability of Right for a DW with coeficients coef. 
    Assuming that the stimulus linearly change the mu coeficitent of the
    potential mu=k*stim. Stim has dimensions Ntrial,Nframes.
    IMPORTANT: stim is a list of ARRAYS
    x0 is the initial position of the decision variable.
    tau: Time constant

    '''

    c2=coef[1]
    c4=coef[2]
    Ntrial=len(stim)

    mu=[k*s for s in stim]

    t=Tframe/tau
    PR=np.zeros(Ntrial)
    for itrial in range(Ntrial):
        coef[0]=mu[itrial][0]
        roots=roots_DW_fast(coef)
        #first frame is special because we need to compute the intial transition    
        if len(roots)==1: #if there is only one stable state
            if roots[0]>0:
                pr=1
            else:
                pr=0
            
        elif len(roots) is not 3:
            print(xL,xR,xM,coef)
            print("complex numbers")
            return -1,-1,-1,-1,-1

        else:

            pr=Initial_transition(coef,roots[0],x0,roots[2],sigma)
            prr,prl=Transition_probabilites(coef,roots[0],roots[1],roots[2],sigma,t)
            pr=pr*prr+(1-pr)*prl
        #Computes the temporal evolution of the pr 
        for iframe in range(1,len(mu[itrial])):
            coef[0]=mu[itrial][iframe]
            roots=roots_DW_fast(coef)

            if len(roots)==1: #if there is only one stable state
                print('only one stable state',itrial,iframe)
                if roots[0]>0:
                    prr=1
                    prl=0
                else:
                    prr=0
                    prl=1
                
            elif len(roots) is not 3:
                print(xL,xR,xM,coef)
                print("complex numbers")
                return -1,-1,-1,-1,-1

            else:
                prr,prl=Transition_probabilites(coef,roots[0],roots[1],roots[2],sigma,t)
            pr=pr*prr+(1-pr)*prl
        PR[itrial]=pr

    return PR

def kiani_fit(param,args):
    '''
    Loglikelihood of the data for the DW and the parameters param
    param[0] k
    param[1] c2
    c4=1
    param[2] sigma
    param[3] tau
    args[0] data, 
        data['coh'] stimulus (NstimxNframes), 
        data['d'] decisions 0,Left 1 Right, data['d'] has dimensicon Nstim,Ntrials
    args[1] Tframe
    The stimulus do not need to have the same frames.
    '''

    k=param[0]
    c2=param[1]
    c4=1
    sigma=param[2]
    tau=param[3]
    data=args[0]
    Tframe=args[1]
    LL=0
    epsilon=1e-10


    PC_DW=PR_DW_diff_number_frames([0,c2,c4],data['coh'],Tframe,sigma,tau=tau,k=k)


    for istim in range(len(data['d'])):
        N=len(data['d'][istim])
        Ncorrect=np.sum(data['d'][istim])
        LL=LL-Ncorrect*np.log(PC_DW[istim]+epsilon)-(N-Ncorrect)*np.log( (1-PC_DW[istim])+epsilon)
    return LL


# def frames_fit(param,args):
#     '''
#     Loglikelihood of the data for the DW and the parameters param
#     param[0] k
#     param[1] c2
#     c4=1
#     param[2] sigma
#     param[3] tau
#     args[0] data, data['coh'] stimulus (NstimxNframes), data['d'] decisions 0 Left 1 Right
#     args[1] Tframe
#     The stimulus do not need to have the same frames.
#     '''
#     k=param[0]
#     c2=param[1]
#     c4=param[2]
#     sigma=0.2#param[2]
#     tau=param[3]
#     data=args[0]
#     Tframe=args[1]
#     LL=0
#     epsilon=1e-10


#     PR_DW=PR_DW_frames([0,c2,c4],data['coh'],Tframe,sigma,tau=tau,k=k)


#     for istim in range(len(data['d'])):
#         if data['d'][istim]==1:
#             LL=LL-np.log(PR_DW[istim]+epsilon)
#         else:
#             LL=LL-np.log( (1-PR_DW[istim])+epsilon)
#     return LL


# def frames_fit_l2(param,args):
#     '''
#     Loglikelihood of the data for the DW and the parameters param
#     param[0] k
#     param[1] c2
#     c4=1
#     param[2] sigma
#     param[3] tau
#     args[0] data, data['coh'] stimulus (NstimxNframes), data['d'] decisions 0 Left 1 Right
#     args[1] Tframe
#     Stimulus do not need to have the same frames.
#     '''

#     k=1
#     c2=param[0]
#     c4=param[1]
#     sigma=param[2]
#     tau=fabs(param[3])

#     data=args[0]
#     Tframe=args[1]
#     lam=args[2]

#     LL=0
#     epsilon=1e-2
#     if len(args)>3:
#         Np=args[3]
#         pool = mpl.Pool(processes=Np)
#         # ARGS: coef,stim,Tframe,sigma,tau,k,x0
#         Nchunk=len(data['coh'])/Np  
#         # ARGS=[ [ [0,c2,c4],data['coh'][ichunk*Nchunk:(ichunk+1)*Nchunk],Tframe,sigma,tau,k,0.] for ichunk in range(Np) ]
#         ARGS=[ [ [0,c2,c4],s,Tframe,sigma,tau,k,0.] for s in data['coh']]
#         # print len(ARGS)
#         aux=pool.map(PR_DW_frames_parallel,ARGS,chunksize=Nchunk)
#         PR_DW=np.hstack(aux)
#         pool.close()

#     else:
#         PR_DW=PR_DW_frames([0,c2,c4],data['coh'],Tframe,sigma,tau=tau,k=k)

#     for istim in range(len(data['d'])):
#         if data['d'][istim]==1:
#             LL=LL-np.log(PR_DW[istim]+epsilon)
#         else:
#             LL=LL-np.log( (1-PR_DW[istim])+epsilon)
#     return LL +lam*(k*k+c2*c2+sigma*sigma+tau*tau),PR_DW


# def energy_landscape(ARGS):
    
#     param=[ARGS[0][0],ARGS[0][1],0.25,10]
#     args=ARGS[1]
#     return frames_fit_l2(param,args)

 
def PR_DW_pulses_delay(coef_stim,coef_NO_stim,stim,Tframe,Tdelay,sigma_s,sigma_i,tau=1,k=1.,x0=0.):
    '''
    Probability of R  in an task with delay Tdelay between frames.
    coef_stim:  DW coeficients during stimulus
    coef_No_stim:  DW coeficients during delay
    stim: mu coeficients for all trials and frames. Dimensions NtrialsxNframes
    sigma_s: Stimulus fluctuations strength
    sigmas_i: Internal_noise strength
    tau: Time constant
    k: the coeficient mu is proportional to the stimulus coherence: mu=k*stim
    '''

    Ntrial=len(stim)
    Npulses=len(stim[0])
    mu=k*stim
    t=Tframe/tau
    PR=np.zeros(Ntrial)
    p=np.zeros(4)


    #Transition probabilities during delay
    roots=roots_DW(coef_NO_stim)
    if len(roots)==3:
        xL=roots[0]
        xR=roots[2]
        xM=roots[1]

        prr_delay,prl_delay=Transition_probabilites(coef_NO_stim,xL,xM,xR,sigma_i,Tdelay/tau)
    else:
        print(coef_NO_stim,roots)
        print('No DW during delay')
    ################Pulses##########################
    for itrial in range(Ntrial):
        for ipulse in range(Npulses):
            p[0]=coef_stim[2]
            p[1]=0
            p[2]=-coef_stim[1]
            p[3]=-mu[itrial][ipulse]
            roots=np.roots(p)
            roots_real_index=np.where(np.imag(roots)==0)
            roots=np.real(roots[roots_real_index])
            if len(roots)==1: #if there is only one stable state
                if roots[0]>0:
                    pr=1
                else:
                    pr=0
                
            elif len(roots) is not 3:
                print(xL,xR,xM)
                print("complex numbers")
                return -1,-1,-1,-1,-1

            else:
                roots=np.sort(roots)
                xL=roots[0]
                xR=roots[2]
                xM=roots[1]
                coef_stim[0]=mu[itrial][ipulse]

                sigma=sqrt(sigma_i**2+sigma_s**2)
                if ipulse==0:
                    pr=Initial_transition(coef_stim,xL,x0,xR,sigma)
                prr,prl=Transition_probabilites(coef_stim,xL,xM,xR,sigma,Tframe/tau)
                pr=pr*prr+(1-pr)*prl

            if ipulse<Npulses-1:
                pr=pr*prr_delay+(1-pr)*prl_delay

 
        PR[itrial]=pr

    return PR



# def minimize_parallel(ARGS):
#     param=ARGS[0]
#     data=ARGS[1]
#     Tframe=ARGS[2]
#     path=ARGS[3]
#     seed=ARGS[4]
    
#     args=[data,Tframe,0]
#     print('icondition',ARGS[-1])

#     res=minimize(frames_fit_l2,param,args=args, method='nelder-mead')

#     i=0
#     fname=path+'optimizer_result_kfix_moresigmas_Nstim'+str(len(data['d']))+'_i'+str(i)+'.pickle'
#     while  os.path.isfile(fname):
#         i=i+1
#         fname=path+'optimizer_result_kfix_moresigmas_Nstim'+str(len(data['d']))+'_i'+str(i)+'.pickle'

#     data_save={'res':res,'lam':0.,'iparam':ARGS[0],'seed':seed}
#     f=open(fname,'wb')
#     pickle.dump(data_save,f)
#     f.close()
    
#     return res.success


# def summary_results(Nstim,path):
#     data_save=[]
#     filenames=glob.glob(path+'optimizer_result_kfix_moresigmas_Nstim'+str(Nstim)+'_i*')
#     for name in filenames:
#         f=open(name,'rb')
#         data=pickle.load(f)
#         data_save.append(data)
#         f.close()

#     f=open(path+'summary_optimizer_result_kfix_Nstim'+str(Nstim)+'.pickle','wb')
#     pickle.dump(data_save,f)
#     f.close()
#     return name

# def summary_results_lam(ilam,Nstim,path):
#     data_save=[]
#     fname=path+'optimizer_result_Nstim'+str(Nstim)+'_l2'+str(ilam)+'_i*'
#     filenames=glob.glob(fname)
#     for name in filenames:
#         f=open(name,'rb')
#         data=pickle.load(f)
#         data_save.append(data)
#         f.close()

#     f=open(path+'summary_optimizer_result_Nstim'+str(Nstim)+'_il2'+str(ilam)+'.pickle','wb')
#     pickle.dump(data_save,f)
#     f.close()
#     return name



# def make_stim(seed,sigmas,Nframes,Nstim):
#     np.random.seed(seed)
#     stim=np.random.randn(Nstim,Nframes)
#     Nstim_sigma=Nstim/len(sigmas)
#     for isigma,s in enumerate(sigmas):
#         stim[isigma*Nstim_sigma:(isigma+1)*Nstim_sigma]=s*stim[isigma*Nstim_sigma:(isigma+1)*Nstim_sigma]

#     return stim

# def make_stim2(seed,sigmas,Nframes,Nstim,mu_max):
#     np.random.seed(seed)
#     stim=np.random.randn(Nstim,Nframes)

#     # stim=np.random.random((Nstim,Nframes))
#     Nstim_sigma=Nstim/len(sigmas)
#     for isigma,s in enumerate(sigmas):
#         stim[isigma*Nstim_sigma:(isigma+1)*Nstim_sigma]=s*stim[isigma*Nstim_sigma:(isigma+1)*Nstim_sigma]
#     aux=np.exp(-stim)
#     return mu_max*(1-aux)/(1+aux)




# def logistic_function(x,beta,b):

#     return 1.0/(1.0+np.exp(-beta*(x-b)))


# def PR_DW_frames_logistic_fit(coef,stim,Tframe,sigma,max_mu,tau=1,k=1.,x0=0.):

#     dt=tau/20.
#     NTframes=int(Tframe/dt)
#     Ntrials=1000
#     Nmu=21
#     MUS=np.linspace(-max_mu,max_mu,Nmu)
#     internal_noise=sigma*np.random.randn(Nmu,Ntrials,NTframes)
#     roots=roots_DW_fast(coef)
#     prr_sim=np.zeros(Nmu)
#     for imu,mu in enumerate(MUS):
#         d,_=sd.simulation_frames_tau(np.array(coef),np.tile(np.array([mu]),(Ntrials,1)),internal_noise[imu], Tframe, tau,roots[2])
#         prr_sim[imu]=np.mean( (np.array(d)+1)/2)

#     popt,pcov=curve_fit(logistic_function,MUS,prr_sim,p0=[3,-1])

#     PRR=logistic_function(stim,popt[0],popt[1])
#     PRL=1-logistic_function(-stim,popt[0],popt[1])
#     rootsI= [ roots_DW_fast( [stim[i][0], coef[1],coef[2]] ) for i in range(len(stim)) ] 
#     PR0= np.array( [ Initial_transition2( [stim[i][0], coef[1],coef[2]] , rootsI[i], x0, sigma) for i in range(len(stim)) ] )

#     PRt=PR0*PRR[:,0]+(1-PR0)*PRL[:,0]
#     for iframe in range(len(stim[0])):
#         PRt=PRt*PRR[:,iframe] +(1-PRt)*PRL[:,iframe]
#     return PRt,popt


# def PR_DW_frames_logistic_fit(coef,stim,Tframe,sigma,max_stim,tau=1,k=1.,x0=0.):

#     dt=tau/20.
#     NTframes=int(Tframe/dt)
#     Ntrials=1000
#     Nmu=21
#     internal_noise=sigma*np.random.randn(Nmu,Ntrials,NTframes)
    
#     mu_max_attractors=max_mu([0,coef[1],coef[2]])
#     MUS=np.linspace(-3*mu_max_attractors,3*mu_max_attractors,Nmu)

#     roots_mu_max1=roots_DW_fast([mu_max_attractors,coef[1],coef[2]])
#     roots_mu_max2=roots_DW_fast([-mu_max_attractors,coef[1],coef[2]])

#     prr_sim=np.zeros(Nmu)
#     for imu,mu in enumerate(MUS):
#         roots=roots_DW_fast(coef)
#         if len(roots)==3:
#             x0=roots[2]
#         if len(roots)==1:
#             if mu>0:
#                 x0=roots_mu_max1[2]
#             else:
#                 x0=roots_mu_max2[2]

#         d,_=sd.simulation_frames_tau(np.array(coef),np.tile(np.array([mu]),(Ntrials,1)),internal_noise[imu], Tframe, tau,x0)
#         prr_sim[imu]=np.mean( (np.array(d)+1)/2)

#     popt,pcov=curve_fit(logistic_function,MUS,prr_sim,p0=[3,-1])

#     PRR=logistic_function(stim,popt[0],popt[1])
#     PRL=1-logistic_function(-stim,popt[0],popt[1])
#     rootsI= [ roots_DW_fast( [stim[i][0], coef[1],coef[2]] ) for i in range(len(stim)) ] 
#     PR0= np.array( [ Initial_transition2( [stim[i][0], coef[1],coef[2]] , rootsI[i], x0, sigma) for i in range(len(stim)) ] )

#     PRt=PR0*PRR[:,0]+(1-PR0)*PRL[:,0]
#     for iframe in range(len(stim[0])):
#         PRt=PRt*PRR[:,iframe] +(1-PRt)*PRL[:,iframe]
#     return PRt,popt