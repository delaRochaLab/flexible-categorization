"""
@author: genis
This script computes the psychophysical kernel of the Double Well
model for different magnitudes of the stimulus fluctuations.
"""

import sys
sys.path.insert(0, '/home/genis/flexible-categorization/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt

PK=[]
NPKA=[]
PRI=[]

Nstim=int(1e5)
Nsigmas=50
T=1000
tau=200.
dt=tau/40.

NT=int(T/dt)
time=np.linspace(0,T,NT)

sigma_i=0.1
c2=2
c4=4

#############      KERNEL sigmas        ######################
SIGMAS_S=np.linspace(0.02,1.0,Nsigmas)

for isigma in range(len(SIGMAS_S)):
    stim_noise=SIGMAS_S[isigma]*np.random.randn(Nstim,NT)
    internal_noise=sigma_i*np.random.randn(Nstim,NT) #internal nopise
    coef=np.array([0.0,c2,c4])
    d,xfinal=sd.simulation_DW(coef,stim_noise,internal_noise,T,tau,0.)
    d=np.array(d)
    print('computing kernel DW',isigma)
    kernel=ba.kernel_err(stim_noise,d,error=False)
    PK.append(kernel['kernel'])
    PRI.append(ba.PK_slope(PK[-1]) )
    NPKA.append(ba.total_area_kernel_PInormalize(PK[-1]))


for isigma in range(len(SIGMAS_S)):

    plt.plot(time,PK[isigma],color=hp.colormap('hot',isigma,0,len(SIGMAS_S)+2))




path="/home/genis/flexible-categorization/figure2/"
data={ "PK":PK,"PRI":PRI,"NPKA":NPKA,"SIGMAS_S":SIGMAS_S,"T":T,
"sigma_i":sigma_i,"c2":c2,"c4":c4,"tau":tau}


f=open(path+"pk_sigmas.pickle","wb")
pickle.dump(data,f)
f.close()




