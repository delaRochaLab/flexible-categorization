"""
@author: genis
This script computes the psychophysical kernel of the Double Well
model for different stimulus durations
"""

import sys
sys.path.insert(0, '/home/genis/flexible-categorization/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt

PK=[]
NPKA=[]
PRI=[]

Nstim=int(1e5)
NstimT=51
Nsigmas=50
tau=200.
dt=tau/40.
sigma_i=0.1
c2=2
c4=4

#############      KERNEL T        ######################
SIGMAS_S=np.linspace(0.02,1.0,Nsigmas)
isigma=28
sigma_s=SIGMAS_S[isigma]
STIM_T=np.linspace(250,2500,NstimT)
for iT, T in enumerate(STIM_T):
    NT=int(T/dt)
    time=np.linspace(0,T,NT+1)
    stim_noise=sigma_s*np.random.randn(Nstim,NT)
    internal_noise=sigma_i*np.random.randn(Nstim,NT) #internal nopise
    coef=np.array([0.0,c2,c4])
    d,xfinal=sd.simulation_DW(coef,stim_noise,internal_noise,T,tau,0.)
    d=np.array(d)
    print('computing kernel DW',iT)
    kernel=ba.kernel_err(stim_noise,d,error=False)
    PK.append(kernel['kernel'])
    PRI.append(ba.PK_slope(PK[-1]) )
    NPKA.append(ba.total_area_kernel_PInormalize(PK[-1]))


for iT, T in enumerate(STIM_T):
    NT=int(T/dt)
    time=np.linspace(0,T,NT)
    plt.plot(time,PK[iT],color=hp.colormap('winter',iT,0,NstimT+2))




path="/home/genis/flexible-categorization/figure2/"
data={ "PK":PK,"PRI":PRI,"NPKA":NPKA,"STIM_T":STIM_T,"sigma_s":sigma_s,
"sigma_i":sigma_i,"c2":c2,"c4":c4,"tau":tau}


f=open(path+"pk_T.pickle","wb")
pickle.dump(data,f)
f.close()




