#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 10:51:54 2018

@author: genis
"""
import sys
sys.path.insert(0, '/home/genis/flexible-categorization/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt

plt.rcParams['svg.fonttype'] = "none"
plt.rcParams['pdf.fonttype'] = 'type3'

path="/home/genis/flexible-categorization/figure2/"
f=open(path+"pk_sigmas.pickle","rb")
data=pickle.load(f)
f.close()
SIGMAS_S=data["SIGMAS_S"]
tau=data["tau"]
NPKA=data["NPKA"]
PRI=data["PRI"]
sigma_i=data["sigma_i"]
c2=data["c2"]
c4=data["c4"]
T=data["T"]
PK=data["PK"]
################ DW kernel vs sigma ##################

color_sigma=[ "#560105","#8B1518","#C1332D","#F55546"]
color_sigma_i=[  "#4D3F07","#826C17","#BB9D2C","#F8D046"]
color_T=["#0E2644","#2C4E7E","#5279B8","#7EA7F2"]
color_line='black'


fontsize=8
linewidth=2           
isigmas=[0,4,28,49]

fig, axes = plt.subplots(nrows=3, ncols=5,figsize=(17/2.54,10/2.54))
Nstim=10

####### PLOT TRACES simga #######################
np.random.seed(41178)
c=['black','dimgrey','silver']
indices=[1,2,1]


# cm = plt.get_cmap('hot') 
# cNorm  = colors.Normalize(vmin=0, vmax=6)
# scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=cm)
 

icol=[0,2]
isigmas_traces=[4,28]

itraces=[0,8]
dt=data["tau"]/40
NT=int(T/dt)
coef=np.array([0.0,c2,c4])
internal_noise=sigma_i*np.random.randn(Nstim,NT)
stim_aux=np.random.randn(Nstim,NT)

itrace=6

for i,isigma in enumerate(isigmas_traces):
        stim_noise=SIGMAS_S[isigma]*stim_aux
        d,x_traces=sd.simulation_DW_traces(coef,stim_noise,internal_noise,T,tau,0.)

        x_time=np.linspace(0,T,NT+1)
        x_traces=np.array(x_traces)
        if i==0:
            axes[0][0].plot(x_time,x_traces[itrace],'-',linewidth=1,color=color_sigma[icol[i]])
        else:
            axes[1][0].plot(x_time,x_traces[itrace],'-',linewidth=1,color=color_sigma[icol[i]])
         


######### LABELS ######### 
hp.remove_axis_bottom(axes[0][0])
hp.remove_axis_bottom(axes[1][0])


axes[0][0].set_ylabel('Decision variable x',fontsize=fontsize)
axes[0][0].yaxis.set_label_coords(-0.25,0.15)


axes[1][0].set_xlabel('')
axes[0][0].set_xlabel('')

axes[0][0].spines['left'].set_visible(False)
axes[1][0].spines['left'].set_visible(False)



######### extralines ######### 
axes[0][0].axhline(0,0,x_time[-1],color='grey',linestyle='dashed',linewidth=0.5)
axes[1][0].axhline(0,0,x_time[-1],color='grey',linestyle='dashed',linewidth=0.5)

######### ticks ######### 

hp.yticks(axes[0][0],[0],fontsize=fontsize)
hp.yticks(axes[1][0],[0],fontsize=fontsize)

axes[1][0].set_ylim(-0.8,1)
axes[0][0].set_ylim(-0.8,1)


######## Potentials ##########
ax_potential=fig.add_axes([0.1,.75,0.1,0.06])
hp.plot_potential(ax_potential,'DW',coef=[0,1,2],xmin=-1.4,xmax=1.4,ymin=-0.2,ymax=0.3,axis_off=True,color_left='dimgrey',color_right='dimgrey')





########## kernel DW for different sigmas ##############

cmap=['hot',0,len(isigmas)+2,0]
icolors=[0,1,2,3]
for icolor,isigma in enumerate(isigmas):
    cmap[-1]=icolors[icolor]
    hp.kernel_plot(axes[0][1+icolor],PK[isigma],1.,color=color_sigma[icolor],smoothing_window=5,linewidth=linewidth,yticks=[0.5,0.55,0.6],yticklabels=['0.5','0.55','0.6'],fontsize=fontsize)        
    axes[0][1+icolor].set_ylim(0.48,0.6)
    hp.remove_axis_bottom(axes[0][1+icolor])
    axes[0][1+icolor].axhline(0.5,0,x_time[-1],color='grey',linestyle='dashed',linewidth=0.5)
    axes[0][1+icolor].set_xlabel('')    
    if icolor!=0:
        axes[0][1+icolor].set_ylabel('')
        axes[0][1+icolor].axis('off')
    


axes[0][1].set_ylabel("Stimulus impact")



########## kernel DW for different times ##############

f=open(path+"pk_T.pickle","rb")
data=pickle.load(f)
PK_T=data["PK"]
PRI_T=data["PRI"]
NPKA_T=data["NPKA"]
STIM_T=data["STIM_T"]
f.close()



IT=[6,17,49]
tlabel=[.5,1,2.5]


for icolor,it in enumerate(IT):
	x_time=np.linspace(0,STIM_T[it],len(PK_T[it]))
	hp.kernel_plot(axes[1][1+icolor],PK_T[it],tlabel[icolor],color=color_T[icolor],smoothing_window=5,linewidth=linewidth,yticks=[0.5,0.55,0.6],yticklabels=['0.5','0.55','0.6'],fontsize=fontsize,ylim=0.6)
	hp.remove_axis_bottom(axes[1][1+icolor])
	axes[1][1+icolor].set_xlabel('')
	if icolor!=0:
	    axes[1][1+icolor].set_ylabel('')
	    axes[1][1+icolor].axis('off')

	axes[1][1+icolor].axhline(0.5,0,STIM_T[it],color='grey',linestyle='dashed',linewidth=0.5)



######### LABELS ######### 
hp.remove_axis(axes[1][1])

axes[0][1].set_ylabel('')
axes[1][1].yaxis.set_label_coords(-0.8,1.2)

### Primancy vs recency index  and total area for sigma####

axes[2][1].axis('off')
markersize=3

ws=0.1
h0=0.125
hf=0.15
w0=0.1
wf=0.025
hs_special=0.2

hinset=0.125
w=(1-w0-wf-3*ws)/4
hs=0.12
h=0.5-h0-hs

######### set positions of the axes ############


ax1=fig.add_axes([w0,h0,w,h])
ax2=fig.add_axes([w0+w+ws,h0,w,h])

ax1_time=fig.add_axes([w0+2*(w+ws),h0,w,h])
ax2_time=fig.add_axes([w0+3*(w+ws),h0,w,h])



isigma_min=0
isigma_max=49


ax2.plot(SIGMAS_S[isigma_min:isigma_max],NPKA[isigma_min:isigma_max],'-',color=color_line,markersize=markersize,linewidth=linewidth)         

ax1.plot(SIGMAS_S[isigma_min:isigma_max],-np.array(PRI[isigma_min:isigma_max]),'-',color=color_line,markersize=markersize,linewidth=linewidth)         
ax1.plot(SIGMAS_S,np.zeros(len(SIGMAS_S)),'--',color='grey',lw=0.5)

ax1.plot([sigma_i,sigma_i],[-1,-0.95],'-',color='black',lw=0.5)
ax2.plot([sigma_i,sigma_i],[0,0.05],'-',color='black',lw=0.5)

for i,isigma in enumerate(isigmas):
    ax2.plot(SIGMAS_S[isigma],NPKA[isigma],'o',markersize=markersize,color=color_sigma[i])
    ax1.plot(SIGMAS_S[isigma],-PRI[isigma],'o',markersize=markersize,color=color_sigma[i])

hp.remove_axis(ax1)
hp.remove_axis(ax2)


      
ax2.set_xlabel(r'Stim fluctuations $\sigma_s$',fontsize=fontsize)
ax1.set_xlabel(r'Stim fluctuations $\sigma_s$',fontsize=fontsize)
ax1.set_ylabel(r'Normalized PK slope',fontsize=fontsize)      
hp.yticks(ax2,[0,0.5,1],['0','0.5','1'],fontsize=fontsize)
hp.yticks(ax1,[-1,0,1],['-1','0','1'],fontsize=fontsize)
hp.xticks(ax1,[0,0.5,1],fontsize=fontsize)
hp.xticks(ax2,[0,0.5,1],fontsize=fontsize)
ax2.set_ylabel(r'Normalized PK area',fontsize=fontsize)
axes[1][2].axis('off')


## Pk slope  and total area for different T####
ax2_time.plot(STIM_T,NPKA_T,'-',color=color_line,markersize=markersize,linewidth=linewidth)         
ax1_time.plot(STIM_T,-np.array(PRI_T),'-',color=color_line,markersize=markersize,linewidth=linewidth)         
hp.remove_axis(ax1_time)
hp.remove_axis(ax2_time)    
ax2_time.set_xlabel(r'Stim duration (s)',fontsize=fontsize)
ax1_time.set_xlabel(r'Stim duration (s)',fontsize=fontsize)
ax2_time.set_ylabel(r'Normalized PK area',fontsize=fontsize)
ax1_time.set_ylabel(r'Normalized PK slope',fontsize=fontsize)      
ax1_time.plot([0,STIM_T[-1]],np.zeros(2),'--',color='grey',lw=0.5)
hp.yticks(ax1_time,[-0.5,0,0.5],["-0.5","0","0.5"],fontsize=fontsize)
hp.xticks(ax2_time,[0,1250,2500],['0','1.25','2.5'],fontsize=fontsize)
hp.xticks(ax1_time,[0,1250,2500],['0','1.25','2.5'],fontsize=fontsize)
hp.yticks(ax2_time,[0,0.5,1],['0','0.5','1'],fontsize=fontsize)

h=0.2
h0=0.1
hf=0.05
w0=0.1
wf=0.01
ws=0.15
w0=0.1
ws_kernel=0.01
w=(1-w0-ws-wf-3*ws_kernel)/5.
w_traces=w
hf=0.05
hs=0.05
h=(0.5-hf-hs)/2.
iax=0
axes[iax][0].set_position([w0,1-h-hf ,w_traces, h])

for iax2 in range(1,len(axes[0])):
    axes[iax][iax2].set_position([w0+(iax2-1)*(w+ws_kernel)+ws+w_traces,1-h-hf ,w, h])

wl=[0.5,1,2.5]
wl_sum=[0,0.5,1.5]   
iax=1
axes[iax][0].set_position([w0,1-2*h-hs-hf,w_traces, h])

iax_aux=0
for iax2 in range(1,len(axes[0])-1):
    
    axes[iax][iax2].set_position([ w0+ws+w_traces+(iax2-1)*ws_kernel + wl_sum[iax_aux]*w   ,1-2*h-hs-hf ,wl[iax_aux]*w, h])
    iax_aux+=1
axes[1][4].axis('off')   
for i in range(len(axes[2])):
    axes[2][i].axis('off')   

plt.show()
fig.savefig(path+'figure2_pyhton.svg',format="svg")


 
            




















