#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 17 15:51:36 2018

@author: genis
"""

import sys
sys.path.insert(0, '/home/genis/flexible-categorization/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt
import fit_dw_fc as fdw
from scipy.stats import norm
import scipy.io as sio


path="/home/genis/flexible-categorization/figure5/"

fontsize=8

sigma=["8","16","36"]
fig, axes = plt.subplots(nrows=7, ncols=3,figsize=(17.85/2.54,19/2.54))

icol=1
irow=-1
ITIME=3
tmax=4500


xticks=[500,2500,4500]
xlabels=["0","2","4"]
xticks_k=[0,2000,4000]
itrials=[10,3,9]

color_sigma=["#560105","#8B1518","#C1332D","#F55546"]
color_sigma_i=["#4D3F07","#826C17","#BB9D2C","#F8D046"]
color_T=["#262932","#5B6A88","#89B3F8"]



itrial=[3,17,1]

fig2, axes2 = plt.subplots(nrows=3, ncols=1,figsize=(18/2.54,38/2.54))




for i, s in enumerate(sigma):
	filename=path+'coh_0_sigma_'+s+'/worker1_'+str(itrial[i])+'.mat'
	mat_contents = sio.loadmat(filename)

	rateE1=np.reshape(mat_contents["rateE1"],(6500))
	rateE2=np.reshape(mat_contents["rateE2"],(6500))

	spks_i_E1=np.reshape(mat_contents["spks_i_E1"],(len(mat_contents["spks_i_E1"])))
	spks_i_E2=np.reshape(mat_contents["spks_i_E2"],(len(mat_contents["spks_i_E2"])))

	spks_t_E1=np.reshape(mat_contents["spks_t_E1"],(len(mat_contents["spks_t_E1"])))
	spks_t_E2=np.reshape(mat_contents["spks_t_E2"],(len(mat_contents["spks_t_E2"])))

	stim_E1=np.reshape(mat_contents["I_E1"],len(rateE1))
	stim_E2=np.reshape(mat_contents["I_E2"],len(rateE2))
	
	t=np.hstack(mat_contents["t"])
	color1="#844494ff"
	color2="#a7c439ff"

	tmin=0
	markersize=0.5
	N=200
	irow=irow+1


	rateE1=ba.running_average(rateE1,30,mode='same')
	rateE2=ba.running_average(rateE2,30,mode='same')

	stim_E1=ba.running_average(stim_E1,30,mode='same')
	stim_E2=ba.running_average(stim_E2,30,mode='same')


	axes[irow][icol].plot(t,rateE1,color=color1)
	axes[irow][icol].plot(t,rateE2,color=color2)
	print("rate_plot", irow,icol)

	hp.xticks(axes[irow][icol],xticks,xlabels,fontsize=fontsize)
	axes[irow][icol].set_xlim([tmin,tmax])
	hp.remove_axis_bottom(axes[irow][icol])

	hp.yticks(axes[irow][icol],[0,20,40],["","",""],fontsize=fontsize)
	if i==0:
		axes[irow][icol].set_ylabel("Spikes / s",fontsize=fontsize)
		hp.yticks(axes[irow][icol],[0,20,40],["0","20","40"],fontsize=fontsize)

	irow=irow+1
	axes[irow][icol].plot(t,stim_E1,color=color1)
	axes[irow][icol].plot(t,stim_E2,color=color2)

	hp.xticks(axes[irow][icol],xticks,xlabels,fontsize=fontsize)
	axes[irow][icol].set_xlabel("Time (s)",fontsize=fontsize)

	hp.remove_axis(axes[irow][icol])
	axes[irow][icol].set_xlim([tmin,tmax])
	hp.yticks(axes[irow][icol],[0,0.02,0.04],["","",""])
	if i==0:
		axes[irow][icol].set_ylabel("Stim (pA)",fontsize=fontsize)
		hp.yticks(axes[irow][icol],[0,0.02,0.04],["0","20","40"],fontsize=fontsize)

	###########33 rasters ############
	icol_r=0
	Nraster=50
	markersize=1
	index_spikes_1=np.where(spks_i_E1<Nraster)
	index_spikes_2=np.where(spks_i_E2<Nraster)

	aux1=[ len(np.where(spks_i_E1==j)[0]) for j in range(Nraster)]
	isort1=np.argsort(aux1)

	aux2=[ len(np.where(spks_i_E2==j)[0]) for j in range(Nraster)]
	isort2=np.argsort(aux2)

	for i_order,ineuro in enumerate(isort1):
		index=np.where(spks_i_E1==ineuro)[0]
		axes2[i].plot(spks_t_E1[index],i_order*np.ones(len(index)),'.',fillstyle="full",markersize=markersize,color=color1)
	for i_order,ineuro in enumerate(isort2):
		index=np.where(spks_i_E2==ineuro)[0]
		axes2[i].plot(spks_t_E2[index],i_order*np.ones(len(index))+Nraster,'.',fillstyle="full",markersize=markersize,color=color2)

	axes2[i].set_xlim([tmin,tmax])
	axes[i+1][icol_r].axis("off")
	axes[i+1][icol_r].set_xlim([tmin,tmax])

############ accuracy  #########

isigmas_k=[2,5,9] ### this two are also important for the kernel
icolors=[1,2,3]

filename=path+"acc_vs_sigma_and_T.mat"
mat_contents = sio.loadmat(filename)

acc=np.array(mat_contents["acc"])
sigma=np.hstack(np.array(mat_contents["sigma"]))
T=np.hstack(np.array(mat_contents["t"]))

markersize=3
irow=0
icol=0


acc=np.array(mat_contents["acc"])
sigma=np.hstack(np.array(mat_contents["sigma"]))
T=np.hstack(np.array(mat_contents["t"]))

Ntrials=5000 #The number of trials were 5000 according to Klaus
acc_err=np.zeros(np.shape(acc))
for it in range(len(acc)):
	for isigma in range(len(acc[it])):
		Ncor=int(acc[it][isigma]*Ntrials)
		d=np.concatenate((np.ones(Ncor),np.zeros(Ntrials-Ncor)))
		_,acc_err[it][isigma]=ba.mean_and_error(d)

it_acc=[1,3,5]
labels=["$P_{2s}$","$P_{4s}$","$P_{6s}$"]
for i, it in enumerate(it_acc):
	axes[irow][icol].errorbar(0.25*sigma,acc[it],yerr=acc_err[it],fmt="o-",color=color_T[i],markersize=markersize,lw=2,label=labels[i])
axes[irow][icol].legend(framealpha=0,fontsize=fontsize)



for icolor,isig in enumerate(isigmas_k):
	axes[irow][icol].errorbar([0.25*sigma[isig]],[acc[ITIME][isig]],yerr=[acc_err[ITIME][isig]],fmt="o-",markersize=markersize,lw=2,color=color_sigma[1+icolor])


hp.yticks(axes[irow][icol],[0.55,0.65,0.75,0.85],fontsize=fontsize)
hp.xticks(axes[irow][icol],[0,6,12],fontsize=fontsize)

hp.remove_axis(axes[irow][icol])
axes[irow][icol].set_xlabel("Stimulus fluctuations "+r"$(\sigma_s)$",fontsize=fontsize)
axes[irow][icol].set_ylabel("Accuracy",fontsize=fontsize)

#axes[irow][icol].set_ylabel("Accuracy",fontsize=fontsize)

axes[irow][icol].set_ylim([0.55,0.85])
yticks=[[0.5,0.6,0.7],[0.5,0.55,0.60],[0.5,0.6,0.7]]

#################### kernels ###################

filename_PK=path+"PK_vs_sigma_and_T.mat"
mat_contents_PK = sio.loadmat(filename_PK)
PK=mat_contents_PK["PK"]
PRI=mat_contents_PK["PRI"]

PK_all=[]
for i in range(len(T)):
	PK_all.append(np.array(PK[0][i]))


iplot_kernel=0
icol=2
irow=0
lw_0=0.5
for ipaux,isigma in enumerate(isigmas_k):
	axes[irow][icol].plot(range(len(PK_all[ITIME][isigma])),PK_all[ITIME][isigma],"-",color=color_sigma[1+ipaux] )

	axes[irow][icol].plot([0,4500],[0.5,0.5],'k--',lw=lw_0)
	if ipaux==0:
		hp.yticks(axes[irow][icol],[0.5,0.6,0.7],["0.5","0.6","0.7"],fontsize=fontsize)
		axes[irow][icol].set_ylabel("Stimulus \n impact",fontsize=fontsize)
	hp.yticks(axes[irow][icol],yticks[ipaux],fontsize=fontsize)
	axes[irow][icol].set_ylim([0.5-(yticks[ipaux][-1]-yticks[ipaux][0])*0.2,yticks[ipaux][-1]])
	axes[irow][icol].set_xlim([500,tmax])
	hp.xticks(axes[irow][icol],xticks,xlabels,fontsize=fontsize)
	hp.remove_axis(axes[irow][icol])
	axes[irow][icol].set_xlabel("Time (s)",fontsize=fontsize)
	irow=irow+1


yticks=[[0.5,0.6,0.7],[0.5,0.55,0.60],[0.5,0.55,0.60]]
######################Kernels T #########################

icolors=[1,2,3]

iplot_kernel2=3
isigma=6

itimes=[0,2,5]
xticks=[[500,1000,1500],[500,2000,3500],[500,3500,6500]]
xtick_l=[["0","0.5","1"],["0","1.5","3"],["0","3","6"]]
irow=6
icol=0
yticks_=[[0.5,0.6,0.7],[0.5,0.55,0.6],[0.5,0.55,0.6]]
ylim=[0.7,0.6,0.6]
ylim0=[0.49,0.49]
for ipaux,itime in enumerate(itimes):
	axes[irow][icol].plot(range(len(PK_all[itime][isigma])),PK_all[itime][isigma],"-",color=color_T[ipaux] )

	axes[irow][icol].plot([0,len(PK_all[itime][isigma])],[0.5,0.5],'k--',lw=lw_0)
	axes[irow][icol].set_xlim([500,len(PK_all[itime][isigma])])

	axes[irow][icol].set_ylim([0.49,ylim[ipaux]])
	hp.xticks(axes[irow][icol],xticks[ipaux],xtick_l[ipaux],fontsize=fontsize)
	hp.remove_axis(axes[irow][icol])


	hp.yticks(axes[irow][icol],yticks_[ipaux],fontsize=fontsize)
	if icol==0:
		axes[irow][icol].set_ylabel("Stimulus \n impact",fontsize=fontsize)
	hp.yticks(axes[irow][icol],yticks[ipaux],fontsize=fontsize)

	axes[irow][icol].set_xlabel("Time (s)",fontsize=fontsize)
	icol=icol+1



IROWS=[4,5,3,4,5]
ICOLS=[0,0,2,2,2]

for irow, icol in zip(IROWS,ICOLS):
	axes[irow][icol].axis("off")




#### accuracy position #######
irow=0
icol=0

w0=0.1
ws=0.07
wf=0.02
w=(1-w0-2*ws-wf)/3


hb=0.2
wb=0.25
w0b=1-wb-wf
hf=0.02

axes[irow][icol].set_position([0.5,1-hb-hf,wb,hb])



#### firing, stim and rasters rate position #######


h0=0.07
hs=0.12
hs2=0.04
hs_raster=0.02

hs_kernel_traces=0.04

h=(1-hf-hb-h0-2*hs-hs_raster-hs2-hs_kernel_traces)


h=h/6.
spikis_p=2
rate_p=1
kernel_p=1

for i in range(3):
	axes[i+1][0].set_position([w0+i*(ws+w),1-hf-hb-hs-spikis_p*h,w,spikis_p*h])

	axes[2*i][1].set_position([w0+i*(ws+w),1-hf-hb-hs-(spikis_p+rate_p)*h-hs_raster,w,rate_p*h])
	axes[2*i+1][1].set_position([w0+i*(ws+w),1-hf-hb-hs-(spikis_p+2*rate_p)*h-hs2-hs_raster,w,rate_p*h])
	
#### kernels #####
h_accu=1-hf-hb-hs-(spikis_p+2*rate_p)*h-2*hs2
for i in range(3):
	axes[i][2].set_position([w0+i*(ws+w) ,h_accu-kernel_p*h-hs_kernel_traces,w,kernel_p*h])


#### kernels T #####

h_accu=h_accu-kernel_p*h-hs_kernel_traces
for i in range(3):
	axes[6][i].set_position([w0+i*(ws+w) ,h_accu-hs-kernel_p*h,w,kernel_p*h])


h2=4*h
w2=4*w
hs2=(1-hf-0-3*h2)/2
w02=0.01
for i in range(3):
	axes2[i].set_position([w0,1-hf-h2-i*(hs2+h2),w2,h2])
	axes2[i].axis("off")


fig2.savefig(path+'raster.png',dpi=900)
fig2.show()
fig.show()
fig.savefig(path+'figure5_pyhton.svg')
