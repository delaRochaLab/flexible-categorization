
"""
@author: genis
This scripts computes the consistency of the double well model 
for a single trial as a function of the stimulus fluctuations.
"""
import sys
sys.path.insert(0, '/home/genis/Flexible-Categoritzation/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt

Nstim=100000
Nsigmas=40
T=1000
tau=200
c2=2.
c4=4.
dt=tau/40.
NT=int(T/dt)

coef=np.array([0,c2,c4])

sigmas=np.geomspace(0.05,1.5,Nsigmas)
sigma0s=np.geomspace(0.05,1.0,Nsigmas)
sigma0s=np.insert(sigma0s,0,0)
path="/home/genis/Flexible-Categoritzation/figure4/"


s0=sigma0s[15]

sigmas=np.linspace(0.01,1.5,100)
np.random.seed(152377)
stim_trial=np.random.randn(NT)
stim=np.tile(stim_trial,(Nstim,1))
xfinal_traces=np.zeros((Nstim,NT+1))
d=np.zeros((len(sigmas),Nstim))
c=np.zeros(len(sigmas))

for isigma,s in enumerate(sigmas):
    stim_noise=s*stim
    internal_noise=s0*np.random.randn(Nstim,NT) 
    print(s)
    d[isigma],xfinal_traces=sd.simulation_DW_traces(coef, stim_noise, internal_noise,T,tau,0.)
    d_aux=(d[isigma]+1)/2
    m=np.mean(d_aux)
    c[isigma]=(np.abs(m-0.5)+0.5)

f=open(path+'consistency_single_trial_many_sigmas.pickle','wb')
data={}
data['sigmas']=sigmas
data['s0']=s0
data['cons']=c
pickle.dump(data,f)
f.close()
