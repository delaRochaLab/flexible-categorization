"""
@author: genis
This scripts computes the temporal evolucion of the decision 
variable distribution for a single trial.
"""
import sys
sys.path.insert(0, '/home/genis/Flexible-Categoritzation/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt

Nstim=100000
Nsigmas=40
T=1000
tau=200
c2=2.
c4=4.
dt=tau/40.
NT=int(T/dt)


coef=np.array([0,c2,c4])
sigmas=np.geomspace(0.05,1.5,Nsigmas)
sigma0s=np.geomspace(0.05,1.0,Nsigmas)
sigma0s=np.insert(sigma0s,0,0)
path="/home/genis/Flexible-Categoritzation/figure4/"


s0=sigma0s[15]
sigmas=[0.01,0.25,0.4765,0.8]

np.random.seed(152377)
stim_trial=np.random.randn(NT)

stim=np.tile(stim_trial,(Nstim,1))

xfinal_traces=np.zeros((len(sigmas),Nstim,NT+1))
d=np.zeros((len(sigmas),Nstim))
c=np.zeros(len(sigmas))

for isigma,s in enumerate(sigmas):
    print(isigma)

    stim_noise=s*stim
    internal_noise=s0*np.random.randn(Nstim,NT) 
    d[isigma],xfinal_traces[isigma]=sd.simulation_DW_traces(coef, stim_noise, internal_noise,T,tau,0.)
    d_aux=(d[isigma]+1)/2
    m=np.mean(d_aux)
    c[isigma]=(np.abs(m-0.5)+0.5)

Nbins=100
P=np.zeros((len(sigmas),NT+1,Nbins))
bins=np.linspace(-1.5,1.5,Nbins+1)
for isigma in range(len(sigmas)):
    for it in range(NT+1):
        P[isigma,it,:],_=np.histogram(xfinal_traces[isigma,:,it],bins=bins,density=True)
for isigma in range(len(P)):
    P[isigma]=P[isigma]/np.nanmax(P[isigma])
  
fig,axes=plt.subplots(1,4) 
for iplot in range(len(sigmas)):        
    axes[iplot].imshow(P[iplot].T,aspect='auto',cmap='gray',interpolation=None,vmax=0.5)
    
    
    
f=open(path+'decision_variable_pdf.pickle','wb')
data={}
data['P']=P
data['sigmas']=sigmas
data['s0']=s0
data['cons']=c

pickle.dump(data,f)

f.close()
