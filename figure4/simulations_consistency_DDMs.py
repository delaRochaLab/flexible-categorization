"""
@author: genis
This scripts computes the consistency of the 
canonical models as a function of the stimulus fluctuations. 

"""

import sys
sys.path.insert(0, '/home/genis/Flexible-Categoritzation/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt

Nstim=int(1e3)
Nsigmas=30

sigmas=np.linspace(0.05,1.5,Nsigmas)
sigma0s=np.geomspace(0.05,1.0,Nsigmas)

xfinal1=np.zeros((Nsigmas,Nstim))
xfinal2=np.zeros((Nsigmas,Nstim))

cons=np.zeros((len(sigma0s),Nsigmas))
T=1000
tau=200
dt=tau/40.
NT=int(T/dt)


B=0.5

#model="DDMA"
#model="DDMR"
model="PI"
print(model)

for isigma0, s0 in enumerate(sigma0s):
    print("isigma0",isigma0)
    for isigma,s in enumerate(sigmas):
        print("isigma",isigma)
        stim=np.random.randn(Nstim,T) #stimulus #I add minus here because the equation is -
        stim_noise=s*ba.make_control_stim(0,1,NT,Nstim)
        internal_noise=s0*np.random.randn(Nstim,NT) #internal nopise
        internal_noise2=s0*np.random.randn(Nstim,NT) #internal nopise
        if model=="DDMA":
            d1,xfinal1[isigma]=sd.simulation_DDMA( 0,stim_noise,internal_noise,B,T,tau,0.)
            d2,xfinal2[isigma]=sd.simulation_DDMA( 0,stim_noise,internal_noise2,B,T,tau,0.)
        if model=="DDMR":
            d1,xfinal1[isigma]=sd.simulation_DDMR( 0,stim_noise,internal_noise,B,T,tau,0.)
            d2,xfinal2[isigma]=sd.simulation_DDMR( 0,stim_noise,internal_noise2,B,T,tau,0.)
        if model=="PI":
            d1,xfinal1[isigma]=sd.simulation_PI( 0,stim_noise,internal_noise,T,tau,0.)
            d2,xfinal2[isigma]=sd.simulation_PI( 0,stim_noise,internal_noise2,T,tau,0.)

        d1=np.array(d1)
        d2=np.array(d2)
        cons[isigma0][isigma]=np.mean((d1*d2+1)/2)                                    


data={}
data['sigmas']=sigmas
data['sigma0s']=sigma0s 
data['cons']=cons
data['tau']=tau
data['T']=T
if model!="PI":
    data['bound']=B

path="/home/genis/Flexible-Categoritzation/figure4/"
f=open(path+'consistency_'+model+'_control_mu.pickle','wb')
pickle.dump(data,f)
f.close()


