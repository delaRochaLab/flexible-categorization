"""
@author: genis
This scripts computes the consistency of the 
Double Well model as a function of the stimulus fluctuations. 

"""
import sys
sys.path.insert(0, '/home/genis/Flexible-Categoritzation/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt


Nstim=int(1e3)
Nsigmas=50

sigmas=np.linspace(0.05,1.5,Nsigmas)
sigma0s=[0.14,0.24,0.39,1.0]

xfinal1=np.zeros((Nsigmas,Nstim))
xfinal2=np.zeros((Nsigmas,Nstim))

cons=np.zeros((len(sigma0s),Nsigmas))
T=1000
tau=200
c2=2.
c4=4.
dt=tau/40.
NT=int(T/dt)

coef=np.array([0,c2,c4])

for isigma0, s0 in enumerate(sigma0s):
    print("isigma0",isigma0)
    for isigma,s in enumerate(sigmas):
        print("isigma",isigma)
        stim_noise=s*ba.make_control_stim(0,1,T,Nstim)
        internal_noise=s0*np.random.randn(Nstim,T) #internal nopise
        d1,xfinal1[isigma]=sd.simulation_DW( coef,stim_noise,internal_noise,T,tau,0.)
        d1=np.array(d1)
        internal_noise=s0*np.random.randn(Nstim,T) #internal nopise
        d2,xfinal2[isigma]=sd.simulation_DW( coef,stim_noise,internal_noise,T,tau,0.)
        d2=np.array(d2)
        cons[isigma0][isigma]=np.mean((d1*d2+1)/2)                                    

for isigma0, s0 in enumerate(sigma0s):       
    plt.plot(sigmas,cons[isigma0],'-',color=hp.colormap('hot',isigma0,0,len(sigma0s)*1.2))


data={}
data['sigmas']=sigmas
data['sigma0s']=sigma0s 
data['cons']=cons
data['coef']=coef
data['tau']=tau
data['T']=T

path="/home/genis/Flexible-Categoritzation/figure4/"
f=open(path+'consistency_DW_control_mu.pickle','wb')

pickle.dump(data,f)
f.close()

x0=1
sigma0s=np.insert(sigma0s,0,0)
p_trans=np.zeros((len(sigma0s),Nsigmas))

for isigma0, s0 in enumerate(sigma0s):
    print(isigma0)
    for isigma,s in enumerate(sigmas):

        stim_noise=s*ba.make_control_stim(0,1,T,Nstim)
        internal_noise=s0*np.random.randn(Nstim,T) #internal nopise
        d1,xfinal1[isigma]=sd.simulation_DW( coef,stim_noise,internal_noise,T,tau,x0)
        d1=np.array(d1)
        p_trans[isigma0][isigma]=np.mean((d1+1)/2)                                    

for isigma0, s0 in enumerate(sigma0s):       
    plt.plot(sigmas,p_trans[isigma0],'-',color=hp.colormap('hot',isigma0,0,len(sigma0s)*1.2))


data={}
data['sigmas']=sigmas
data['sigma0s']=sigma0s 
data['p_trans']=p_trans
data['coef']=coef
data['tau']=tau
data['T']=T

f=open(path+'ptrans_DW_control_mu.pickle','wb')

pickle.dump(data,f)
f.close()


