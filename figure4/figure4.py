#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  9 12:46:30 2018

@author: genis
"""

import sys
sys.path.insert(0, '/home/genis/flexible-categorization/functions')

import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt



plt.rcParams['svg.fonttype'] = "none"
plt.rcParams['pdf.fonttype'] = 'type3'

######################Colors#############
color_sigma=[ "#560105","#8B1518","#C1332D","#F55546"]
color_sigma_i=[  "#4D3F07","#826C17","#BB9D2C","#F8D046"]
color_T=["#0E2644","#2C4E7E","#5279B8","#7EA7F2"]
color_PI="black"
color_DDMA="darkcyan"
color_DDMR="silver"

#######################

path="/home/genis/flexible-categorization/figure4/"
f=open(path+'consistency_DW_control_mu.pickle','rb')
data=pickle.load(f)
f.close()


sigmas=data['sigmas']
sigma0s=data['sigma0s']
cons=data['cons']


cmap='viridis'
vmin=0


f=open(path+'consistency_single_trial_many_sigmas.pickle','rb')
data2=pickle.load(f)


f.close()


index_sigma0=range(4)#[10,15,20,29]
vmax=len(index_sigma0)

fig, axes = plt.subplots(nrows=2, ncols=2,figsize=(13/2.54,15./2.54))
color_potential='grey'
fontsize=8

labels=[]
for  isigma0 in index_sigma0:
    labels.append("$\sigma_i$="+str( round(sigma0s[isigma0],2)  ) )

for icolor,isigma in enumerate(index_sigma0):
    axes[0][0].plot(sigmas,cons[isigma],'-',color=color_sigma_i[icolor],lw=2)  

    axes[0][0].plot(np.linspace(0,1,2),np.zeros(2)+0.5,'k--',lw=0.5)
ypos_text=0.95

                 
hp.remove_axis_bottom(axes[0][0])    

#### xaxes ########
axes[0][0].set_xlim(0,1)


#### yaxes ########
axes[0][0].set_ylabel('Consistency',fontsize=fontsize)
hp.yticks(axes[0][0],[0.5,0.75,1],['0.5','0.75','1'],fontsize=fontsize)




#####################transisions probability######################
f=open(path+'ptrans_DW_control_mu.pickle','rb')
data=pickle.load(f)
f.close()

sigmas=data['sigmas']
sigmas_trans=sigmas.copy()
sigma0s=data['sigma0s']
P_trans=1-data['p_trans']

for icolor,isigma0 in enumerate(index_sigma0):

    axes[1][0].plot(sigmas,(P_trans[isigma0+1])-(P_trans[0]),'-',color=color_sigma_i[icolor],lw=2,label=labels[icolor])
    
axes[1][0].legend(frameon=False,fontsize=fontsize)
hp.remove_axis(axes[1][0])    



#### xaxes ########
axes[1][0].set_xlim(0,1)
axes[1][0].plot(np.linspace(0,1,2),np.zeros(2),'k--',lw=0.5)
axes[1][0].set_xlabel(r'Stimulus fluctuations $\sigma_s$',fontsize=fontsize)
hp.xticks(axes[1][0],[0,0.5,1],fontsize=fontsize)


#### yaxes ########

axes[1][0].set_ylabel(r'$P_{R,L}(\sigma_s,\sigma_i)-P_{R,L}(\sigma_s,\sigma_i=0)$',fontsize=fontsize)
hp.yticks(axes[1][0],[0,0.25,0.5],['0','0.25','0.5'],fontsize=fontsize)

w=0.4
h0=0.12
hf=0.05
w0=0.15
hs=0.05
hs_special=0.0

hinset=0.125

h=(1-h0-hf-hs)/2.0
axes[0][0].set_position([w0,1-h-hf,w,h])
axes[1][0].set_position([w0,1-2*h-hf-hs,w,h])




########################## DDDM ###############################
models=['PI','DDMA','DDMR']
index_sigma0=[5]
models_plot=['PI','DDMA','DDMR']
ypos_plot_model=[0.]

w=0.25
h0=0.12
hf=0.12
w0=0.72
hs=0.14
hs_special=0.17

hinset=0.125
hs2=[0.03,0.08,0.08]

h=(0.43-h0-hs)

ax=fig.add_axes([w0,h0,w,h])

colors2=[color_PI,color_DDMA,color_DDMR]
lines=['-','-','--']
for imodel,model in enumerate(models):
    f=open(path+'consistency_'+model+'_control_mu.pickle','rb')
    data=pickle.load(f)
    f.close()

    sigmas=data['sigmas']
    sigma0s=data['sigma0s']
    cons=data['cons']

    for icolor,isigma in enumerate(index_sigma0):
        ax.plot(sigmas,cons[isigma],lines[imodel],color=colors2[imodel],lw=2)

    ax.set_xlim(0,1)

hp.xticks(ax,[0,0.5,1],fontsize=fontsize)
hp.remove_axis(ax)
hp.yticks(ax,[0.5,0.75,1],['0.5','0.75','1'],fontsize=fontsize)
ax.set_xlabel(r'Stimulus fluctuations $\sigma_s$',fontsize=fontsize)
ax.set_ylabel(r'Consistency',fontsize=fontsize)


axes[0][1].axis('off')
axes[1][1].axis('off')


################## P(X,t) ###################################

x_max=1
ymax=10
ymin=-0.08
x_pot=0.915
y_pot=[0.86,0.86-0.145,0.86-2*0.145,0.86-3*0.145]
for i in range(len(y_pot)):
    ax_potential=fig.add_axes([x_pot,y_pot[i],0.075,0.06])
    hp.plot_potential(ax_potential,'DW',coef=[0.0,2,4,0],xmin=-x_max,xmax=x_max,ymin=-0.4,ymax=0.3,axis_off=True,color_left='grey',color_right='grey',linewidth=2)

    
f=open(path+'decision_variable_pdf.pickle','rb')
data={}
data=pickle.load(f)
P=data['P']
f.close()

w=0.2
h0=0.05
hf=0.43
w0=0.7
hs=0.04
hs_special=0.2

hinset=0.125
hs2=[0.03,0.08,0.08]
vmax=0.2
h=(1-h0-hf-3.*hs)/4.0
cons_signle_example=np.zeros(len(P))
for isigma in range(len(P)):
    ax_inset=fig.add_axes([w0,1-h0-hs*isigma-h*(isigma+1),w,h])
    im=ax_inset.imshow(P[isigma].T,aspect='auto',cmap='gray',interpolation=None,vmax=vmax)    
    hp.xticks(ax_inset,[])    
    hp.yticks(ax_inset,[])  
    if isigma==1:
        ax_inset.set_ylabel(r'$P(X,t)$',fontsize=fontsize)

ax_colorbar=fig.add_axes([0.65,0.43,0.01,0.1])
plt.colorbar(im,cax=ax_colorbar,orientation='vertical', ticks=[0,vmax])
ax_colorbar.yaxis.set_ticks_position('left')
 
hp.xticks(ax_inset,[0,100,200],['0','0.5','1'],fontsize=fontsize) 
ax_inset.set_xlabel('Time (s)',fontsize=fontsize)    
   
fontsize_legend=12
fig.text(0.015,0.94,'a',fontsize=fontsize_legend)
fig.text(0.57,0.94,'c',fontsize=fontsize_legend)
fig.text(0.015,0.54,'b',fontsize=fontsize_legend)
fig.text(0.57,0.33,'d',fontsize=fontsize_legend)







################## inset consistency ##############

fontsize_inset=8
h_i=0.1
ax_cons=fig.add_axes([0.27,0.85,w,h_i])

ax_cons.plot(data2['sigmas'][:65],data2["cons"][:65],'k-',lw=1)  

hp.remove_axis(ax_cons)
hp.xticks(ax_cons,[0,0.5,1],fontsize=fontsize_inset)
#hp.xticks(ax_cons,[])
hp.yticks(ax_cons,[0.5,0.75,1],fontsize=fontsize_inset)
ax_cons.set_ylabel("Consistency",fontsize=fontsize_inset)
ax_cons.set_xlabel("$\sigma_s$",fontsize=fontsize_inset)
i_sigmas=[0,16,31,53]
ax_cons.plot(data["sigmas"],data2["cons"][i_sigmas],'ok',markersize=3 )



fig.savefig(path+"figure4_python.svg",format="svg")



plt.show()