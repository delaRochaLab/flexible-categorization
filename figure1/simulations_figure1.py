"""
@author: genis
This script computes the psychophysical kernel for the 
canonical models (DDMA, DDMR and the PI) 
"""

import sys
sys.path.insert(0, '/home/genis/flexible-categorization/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import pickle
import matplotlib.pyplot as plt




############# Parameter definitions ############

kernel={}
kernel['PI']=[]
kernel['DDMA']=[]
kernel['DDMR']=[]
PRR={}
NPKA={}

Nsigmas=30
Nstim=int(1e5)
T=1000
tau=200
dt=tau/40.
NT=int(T/dt)

PRR['PI']=np.zeros(Nsigmas)
PRR['DDMA']=np.zeros(Nsigmas)
PRR['DDMR']=np.zeros(Nsigmas)
NPKA['PI']=np.zeros(Nsigmas)
NPKA['DDMA']=np.zeros(Nsigmas)
NPKA['DDMR']=np.zeros(Nsigmas)

sigma_i=0.1
mu=0.
x0=0.


############# PK  PI ######################
sigmas=np.linspace(0.0025,0.8,Nsigmas)
for isigma in range(len(sigmas)):
    stim=sigmas[isigma]*np.random.randn(Nstim,NT) # stimulus fluctuations
    internal_noise=sigma_i*np.random.randn(Nstim,NT) #internal nopise
    coef_PI=np.array([0.0,0.0,0.0,0.0])
    d,xfinal=sd.simulation_PI(mu,stim,internal_noise,T,tau,x0)
    print('computing kernel PI ',isigma)
    d=np.array(d)
    a=ba.kernel_err(stim,d,error=False)
    kernel['PI'].append(a["kernel"])
    PRR['PI'][isigma]=ba.PK_slope(a["kernel"])
    NPKA['PI'][isigma]=ba.total_area_kernel_PInormalize(a["kernel"])

plt.figure()
plt.plot(kernel['PI'][0])
plt.plot(kernel['PI'][-1])
plt.show()   



#############    PK  DDMA         ######################
B=0.5 

for isigma in range(len(sigmas)):
    print('computing kernel DDMA ',isigma)

    a=np.zeros(Nstim)
    stim=sigmas[isigma]*np.random.randn(Nstim,NT) #stimulus 
    internal_noise=sigma_i*np.random.randn(Nstim,NT) #internal noise
    
    d,xfinal=sd.simulation_DDMA(mu,stim,internal_noise,B,T,tau,x0)
    d=np.array(d)
    a=ba.kernel_err(stim,d,error=False)
    kernel['DDMA'].append(a["kernel"])
    PRR['DDMA'][isigma]=ba.PK_slope(a["kernel"])
    NPKA['DDMA'][isigma]=ba.total_area_kernel_PInormalize(a["kernel"])


plt.figure()
plt.plot(kernel['DDMA'][0])
plt.plot(kernel['DDMA'][-1])
plt.show()   



#############  PK    DDMR        ######################
B=0.5

for isigma in range(len(sigmas)):
    print('computing kernel DDMR ',isigma)
    a=np.zeros(Nstim)
    stim=sigmas[isigma]*np.random.randn(Nstim,NT) 
    internal_noise=sigma_i*np.random.randn(Nstim,NT) #internal nopise
    
    d,xfinal=sd.simulation_DDMR(mu,stim,internal_noise,B,T,tau,x0)
    d=np.array(d)
    a=ba.kernel_err(stim,d,error=False)
    kernel['DDMR'].append(a["kernel"])
    PRR['DDMR'][isigma]=ba.PK_slope(a["kernel"])
    NPKA['DDMR'][isigma]=ba.total_area_kernel_PInormalize(a["kernel"])


plt.figure()
plt.plot(kernel['DDMR'][0])
plt.plot(kernel['DDMR'][-1])
plt.show()   




data_to_save={}
data_to_save['kernel']=kernel
data_to_save['PRI']=PRR
data_to_save['NPKA']=NPKA
data_to_save['sigmas']=sigmas
data_to_save['sigma_i']=sigma_i
data_to_save['tau']=tau
data_to_save['T']=T
data_to_save["B"]=B
data_to_save["x0"]=x0

path="/home/genis/flexible-categorization/figure1/"
f=open(path+'kernels_primacy_recency.pickle','wb')
pickle.dump(data_to_save,f)
f.close()


















