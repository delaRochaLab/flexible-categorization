#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 10:51:54 2018

@author: genis
"""

import sys
sys.path.insert(0, '/home/genis/flexible-categorization/functions')
import analysis_fc as ba
import numpy as np
import simulations_fc as sd
import help_plot as hp
import pickle
import matplotlib.pyplot as plt

path="/home/genis/flexible-categorization/figure1/"
f=open(path+"kernels_primacy_recency.pickle","rb")

data=pickle.load(f)
kernel=data['kernel']
NPKA=data["NPKA"]
PRI=data["PRI"]

f.close()
plt.rcParams['svg.fonttype'] = "none"
plt.rcParams['pdf.fonttype'] = 'type3'

######################Colors#############
#color_sigma=["#EC604D","#BA3A30","#841A19","#4F0304"]
color_sigma=[ "#560105","#8B1518","#C1332D","#F55546"]
color_sigma=[ "#560105","#C1332D","#F55546"]


color_PI="black"
color_DDMA="darkcyan"
color_DDMR="silver"

#######################


# subplot positions 
w=0.3
h=0.225
h0=0.15
hf=0.05
w0=0.15
wf=0.05
ws=(1-2*w-w0-wf)
hs=(1-3*h-h0-hf)/2.

hinset=0.055
models=['DDMA','DDMR','PI']
fontsize=8
linewidth=2           

YLIM=[(0.5,0.54),(0.5,0.58),(0.5,0.58)]
fig, axes = plt.subplots(nrows=4, ncols=4,figsize=(14/2.54,14/2.54))


############# PLOOOT KERNELS ############


isigmas=[[25,10,1],[25,10,1],[25,10,1]] #index sigmas

max_plot=3
for imodel,model in enumerate(models):
    cmap=['hot',0,len(isigmas[0])+2.0,0]
    for icolor,isigma in enumerate(isigmas[imodel]):
        icol=3-icolor
        
        kernel_plot=kernel[models[imodel]][isigma]
        t=np.linspace(0,1,len(kernel_plot))
        hp.kernel_plot(axes[imodel][max_plot-icolor],kernel_plot,1.,color=color_sigma[2-icolor],smoothing_window=5,linewidth=linewidth,yticks=[0.5,0.55,0.6],yticklabels=["",""],ylim=0.6,fontsize=fontsize)
        hp.remove_axis(axes[imodel][max_plot-icolor])
        axes[imodel][max_plot-icolor].set_xlabel('')
        axes[imodel][max_plot-icolor].set_ylabel('')
        axes[imodel][max_plot-icolor].set_ylabel('')
        hp.remove_axis_bottom(axes[imodel][2-icolor+1])

        if 2-icolor+1 > 1:
            axes[imodel][max_plot-icolor].spines['left'].set_visible(False)
            axes[imodel][max_plot-icolor].set_yticks([])
    axes[1][1].set_ylabel('Stimulus Impact',fontsize=fontsize)
    axes[imodel][1].set_yticklabels(['0.5','0.55','0.6'],fontsize=fontsize)
    
    


############# PLOOOT TRACES ############


Nstim=10
T=data["T"]
tau=data["tau"]
sigma_i=data["sigma_i"]
sigmas=data["sigmas"]
x0=data["x0"]
B=data["B"]
dt=tau/40.
NT=int(T/dt)

np.random.seed(45512)
stim=np.random.randn(Nstim,NT)
internal_noise=np.random.randn(Nstim,NT)

bounds=[0,-0.5,0.5]
MUS=[-0.15,0,0.15]

ls=4
hs=8
indices=[[ls,hs],[ls,hs],[ls,hs]]





i_colors=[0,1]
isigmas_plot=[[2,10],[2,10],[2,10]]

for imodel, bound in enumerate(bounds):
    axes[imodel][0].axhline(0,0,1,color='grey',linestyle='dashed',linewidth=0.5)
    axes[imodel][0].spines['left'].set_visible(False)
    for icol,isigma in enumerate(isigmas_plot[imodel]):
        
        stim_noise=sigmas[isigma]*stim
        i_noise=sigma_i*internal_noise
        if imodel==0:
            d,x_traces=sd.simulation_DDMA_traces(0,stim_noise,i_noise,B,T,tau,x0)
            x_traces=np.array(x_traces)
            ind=np.where(x_traces>B)
            x_traces[ind]=B
            ind=np.where(x_traces<-B)
            x_traces[ind]=-B
        elif imodel==1:
            d,x_traces=sd.simulation_DDMR_traces(0,stim_noise,i_noise,B,T,tau,x0)

        elif imodel==2:
            d,x_traces=sd.simulation_PI_traces(0,stim_noise,i_noise,T,tau,x0)

        x_time=np.linspace(0,T,NT+1)
        
        axes[imodel][0].plot(x_time,x_traces[indices[imodel][icol]],'-',linewidth=1,color=color_sigma[i_colors[icol]])


        hp.remove_axis(axes[imodel][0])

axes[0][0].set_ylabel('')
axes[2][0].set_ylabel('')

lw_traces=0.5
axes[0][0].set_yticklabels(['0'],fontsize=fontsize)
axes[1][0].set_yticklabels(['','0',''],fontsize=fontsize)
axes[2][0].set_yticklabels(['','0',''],fontsize=fontsize)
axes[1][0].axhline(0.5,0,x_time[-1],color='grey',linestyle='dashed',linewidth=lw_traces)
axes[1][0].axhline(-0.5,0,x_time[-1],color='grey',linestyle='dashed',linewidth=lw_traces)
axes[0][0].axhline(0.5,0,x_time[-1],color='grey',linestyle='dashed',linewidth=lw_traces)
axes[0][0].axhline(-0.5,0,x_time[-1],color='grey',linestyle='dashed',linewidth=lw_traces)
axes[0][0].set_yticks([0])
axes[1][0].set_yticks([0])
axes[2][0].set_yticks([0])
axes[1][0].set_yticklabels(['0'],fontsize=fontsize)
axes[2][0].set_yticklabels(['0'],fontsize=fontsize)

axes[1][0].set_ylim(-0.6,0.6)
axes[0][0].set_ylim(-0.6,0.6)




##### xticks #########
axes[0][0].set_xticks([0,0.5,1])
axes[1][0].set_xticks([0,0.5,1])
axes[2][0].set_xticks([0,0.5,1])

axes[0][0].set_xticklabels(['','',''],fontsize=fontsize)
axes[2][0].set_xticklabels(['','',''],fontsize=fontsize)
axes[1][0].set_xticklabels(['','',''],fontsize=fontsize)

axes[1][0].set_ylabel('Decision variable x',fontsize=fontsize)
hp.remove_axis_bottom(axes[2][0])

axes[1][0].tick_params(axis='x',which='both',bottom='off',top='off',labelbottom='off')
axes[0][0].spines['bottom'].set_visible(False)
axes[0][0].tick_params(axis='x',which='both',bottom='off',top='off',labelbottom='off')
axes[1][0].spines['bottom'].set_visible(False)





### Primancy vs recency ratio  and total area####

c=[color_DDMA,color_DDMR,color_PI]


w0=0.1
wf=0.025
ws=0.15
w=(1-ws-w0-wf)/2
ax_area=fig.add_axes([0.1,0.09,w,0.22])
ax_pri=fig.add_axes([0.6,0.09,w,0.22])


markersize=2
for imodel,model in enumerate(models):
    if imodel==1:
        ls='dashed'
    else:
        ls='solid'
    ax_area.plot(sigmas,NPKA[model],'-',color=c[imodel],markersize=markersize,linewidth=linewidth,ls=ls)         

ax_area.plot([sigma_i,sigma_i],[0,0.05],color='k')
ls='solid'       
for imodel,model in enumerate(models):
    ax_pri.plot(sigmas[1:],PRI[model][1:],'-',color=c[imodel],markersize=markersize,linewidth=linewidth,ls=ls)         

# We change the definition of PRI to normalized slope

ax_pri.set_ylim([0,1])


ax_pri.plot([sigma_i,sigma_i],[-1,-0.95],color='k')

        
ax_pri.set_xlabel('Stimulus fluctuations '+r'$\sigma_s$',fontsize=fontsize)
ax_area.set_xlabel('Stimulus fluctuations '+r'$\sigma_s$',fontsize=fontsize)

ax_area.set_ylabel(r'Normalized PK area',fontsize=fontsize)
ax_pri.set_ylabel(r'Normalized PK slope',fontsize=fontsize)      

hp.remove_axis(ax_area)
hp.remove_axis(ax_pri)


hp.yticks(ax_area,[0,0.5,1],yticklabels=['0','0.5','1'],fontsize=fontsize)




hp.yticks(ax_pri,[-1,0,1],fontsize=fontsize)



hp.xticks(ax_area,[0,0.3,0.6],fontsize=fontsize)
hp.xticks(ax_pri,[0,0.3,0.6],fontsize=fontsize)






h=0.13
h0=0.1
hf=0.05
w0=0.1
wf=0.025
ws_kernel=0.01
ws0=0.15
w=(1-ws0-w0-wf-2*ws_kernel)/4.
w_traces=w
hs=(0.8-3*h-hf-h0)/3.

for iax in range(len(axes)-1):
    axes[iax][0].set_position([w0,1-h*(1+iax)-hs*(iax)-hf ,w_traces, h])

    for iax2 in range(1,len(axes[0])):
        axes[iax][iax2].set_position([w0+w_traces+ws0+(iax2-1)*(w+ws_kernel),   1-h*(1+iax)-hs*(iax)-hf ,w, h])
    


########## ading potentials ##############
x_pot=w0+w+ws0+w/2-0.05
y_pot=[0.88,0.67,0.44]
fontsize2=8
fig.text(0.07,0.97,'DDM Absorbing',fontsize=fontsize2)
fig.text(0.07,0.76,'DDM Reflecting',fontsize=fontsize2)
fig.text(0.07,0.54,'Perfect Integrator',fontsize=fontsize2)



model=['DDMA','DDMR',"PI"]
color_pot='dimgrey'
for i in range(len(model)):
    ax_potential=fig.add_axes([x_pot,y_pot[i],0.1,0.1])
    hp.plot_potential(ax_potential,model[i],xmin=-1.5,xmax=1.5,ymin=-0.4,ymax=0.4,axis_off=True,color_left=color_pot,color_right=color_pot,coef=[0.05],linewidth=2)

x_pot=0.2
y_pot=[0.17,0.14,0.08]
model=['PI','DDMA','DDMR']
c=[color_PI,color_DDMA,color_DDMR]

color_potentials=[]
for i in range(len(models)):
    ax_potential=fig.add_axes([x_pot,y_pot[i],0.07,0.07])
    hp.plot_potential(ax_potential,model[i],xmin=-1.5,xmax=1.5,ymin=-0.4,ymax=0.4,axis_off=True,color_left=c[i],color_right=c[i],coef=[0],linewidth=2)





  

axes[3][0].axis('off')
axes[3][1].axis('off')
axes[3][2].axis('off')
axes[3][3].axis('off')



plt.show()



fig.savefig('/home/genis/flexible-categorization/figure1/figure1_python.svg')





 
            




















